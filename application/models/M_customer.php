<?php

class M_customer extends CI_Model
{

    private $table = 'customer';

    public function adminMemberListing()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        // $this->db->join('customer', 'customer.id_customer = cars.id_user');
        return $this->db->get();
    }

    public function adminSellerListing()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('customer_type', 'seller');
        return $this->db->get();
    }
}
