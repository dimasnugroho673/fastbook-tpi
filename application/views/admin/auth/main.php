<!DOCTYPE html>
<html lang="en">

<head>

    <title><?= $title ?></title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/admin/dist/'); ?>assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url('assets/admin/dist/'); ?>assets/css/style.css">

    <!-- font -->
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <style>
        * {
            font-family: 'Ubuntu', sans-serif;
        }

        .page-header {
            box-shadow: none;
        }

        .pcoded-main-container,
        .pcoded-content {
            background-color: white;
        }

        .card {
            box-shadow: none !important;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }

        .card:hover {
            box-shadow: none;
        }

        .header-title {
            font-size: 28px;
        }
    </style>


</head>

<?php $this->load->view('admin/' . $view_page); ?>

<!-- Required Js -->
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/vendor-all.min.js"></script>
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/bootstrap.min.js"></script>
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/waves.min.js"></script>



</body>

</html>