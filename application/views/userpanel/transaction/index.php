<div class="card">
    <div class="card-header">
        <h4>Daftar Transaksi</h4>
    </div>
    <div class="card-body">


        <div class="my-3">
            <?= $this->session->flashdata('status') ?>
        </div>

        <?php $check_payment_db = $this->db->get_where(
            'invoice',
            array(
                'for_seller'   => $this->db->get_where(
                    'customer',
                    array(
                        'email' => $this->session->userdata('email')
                    )
                )->row()->id_customer,
                'date_payment' => null,
                'limit_date_payment <' => time()
            )
        );

        $check_expired_db = $this->db->get_where(
            'invoice',
            array(
                'for_seller'   => $this->db->get_where('customer', array(
                    'email' => $this->session->userdata('email')
                ))->row()->id_customer,
                'date_payment' => null,
                'limit_date_payment <' => time()
            )
        );

        $check_success_db = $this->db->get_where(
            'invoice',
            array(
                'for_seller'   => $this->db->get_where(
                    'customer',
                    array(
                        'email' => $this->session->userdata('email')
                    )
                )->row()->id_customer,
                'date_payment !=' => null,
                'status_payment' => 'success'
            )
        );

        $check_waiting_db = $this->db->get_where(
            'invoice',
            array(
                'for_seller'   => $this->db->get_where(
                    'customer',
                    array(
                        'email' => $this->session->userdata('email')
                    )
                )->row()->id_customer,
                'date_payment' => null,
                'limit_date_payment >' => time(),
                'status_payment' => 'waiting'
            )
        );
        // $check_expired_db = $check_expired_db->limit_date_payment;
        ?>

        <div class="row">
            <div class="col-md-12">
                <div class="my-3">
                    <!-- <button type="button" class="btn btn-sm btn-default">
                        Belum dibayar <span class="badge badge-light"><?php echo $check_payment_db->num_rows(); ?></span>
                    </button> -->

                    <button type="button" class="btn btn-sm btn-danger ml-1">
                        Expired <span class="badge badge-light"><?php echo $check_expired_db->num_rows(); ?></span>
                    </button>

                    <button type="button" class="btn btn-sm btn-warning ml-1">
                        Menunggu <span class="badge badge-light"><?php echo $check_waiting_db->num_rows(); ?></span>
                    </button>

                    <button type="button" class="btn btn-sm btn-success ml-1">
                        Sukses <span class="badge badge-light"><?php echo $check_success_db->num_rows(); ?></span>
                    </button>
                </div>
            </div>
        </div>




        <div class="table-reponsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Aksi</th>
                        <th>Kode invoice</th>
                        <th>Customer</th>
                        <th>Tanggal pembayaran</th>
                        <th>Batas pembayaran</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($transactions)) { ?>
                        <?php $no = 1;
                        foreach ($transactions as $t) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <?php if ($t->status_payment != 'success') { ?>
                                                <small><a class="dropdown-item" href="<?= site_url('panel/transaction/confirm_payment/' . urlencode(encrypt_url($t->id_invoice))) ?>">Konfirmasi pembayaran</a></small>
                                            <?php } ?>
                                            <small><a class="dropdown-item" href="#">Another action</a></small>
                                            <small><a class="dropdown-item" href="#">Something else here</a></small>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="text-monospace"><?= $t->id_invoice ?></span></td>

                                <td><?= $t->name_customer ?></td>
                                <td><?php if ($t->date_payment == null) {
                                        echo '<strong>Belum dibayar</strong>';
                                    } else {
                                        echo "<strong>" . date('d F Y H:i:s', $t->date_payment) . "</strong>";
                                    }  ?></td>
                                <td><?= date('d F Y H:i:s', $t->limit_date_payment)  ?></td>
                                <td><?php
                                    if ($t->status_payment == 'waiting') {
                                        if (time() <= $t->limit_date_payment) {
                                            echo '<span class="badge badge-warning">Menunggu</span>';
                                        } elseif (time() > $t->limit_date_payment) {
                                            echo '<span class="badge badge-danger">Expired</span>';
                                        }
                                    } elseif ($t->status_payment == 'success') {
                                        echo '<span class="badge badge-success">Selesai</span>';
                                    }

                                    $t->status ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>
            <?php if (empty($transactions)) { ?>
                <div class="alert alert-danger text-center" role="alert">
                    Belum ada transaksi dilakukan
                </div>
            <?php } ?>
        </div>

    </div>
</div>