<div class="card">
    <div class="card-header" style="border-bottom: none; background: none;">
        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;">Tambah mobil baru</h5>
    </div>
    <div class="card-body">



        <form class="mt-4" action="<?= site_url('panel/car/edit/' . urlencode(encrypt_url($car->id_car))) ?>" method="post" enctype="multipart/form-data">
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="plat_number">No Plat</label>
                        <input type="text" name="plat_number" id="plat_number" class="form-control" placeholder="BP 2382 MX" value="<?= $car->plat_number ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label for="name_car">Nama Mobil</label>
                        <input type="text" name="name_car" id="name_car" class="form-control" placeholder="Cth. Grand Xenia" value="<?= $car->name_car ?>">
                    </div>
                </div>

            </div>
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="vendor">Vendor</label>
                        <input type="text" name="vendor" id="vendor" class="form-control" placeholder="Cth. Toyota/Suzuki" value="<?= $car->vendor ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="year_of_assembly">Tahun Keluaran</label>
                        <input type="number" name="year_of_assembly" id="year_of_assembly" class="form-control" placeholder="Cth. 2019" value="<?= $car->year_of_assembly ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="machine_type">Tipe Mesin</label>
                        <select class="custom-select" name="machine_type" id="machine_type">
                            <option disabled selected>Mesin...</option>
                            <option value="Diesel" <?= ($car->machine_type == "Diesel") ? "selected" : null; ?>>Diesel</option>
                            <option value="Bensin" <?= ($car->machine_type == "Bensin") ? "selected" : null; ?>>Bensin</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="color">Warna</label>
                        <input type="text" name="color" id="color" class="form-control" placeholder="Red/Jet black" value="<?= $car->color ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="transmission">Transmisi</label>
                        <select class="custom-select" name="transmission" id="transmission">
                            <option disabled selected>Transmisi...</option>
                            <option value="Automatic" <?= ($car->transmission == "Automatic") ? "selected" : null; ?>>Automatic</option>
                            <option value="Manual" <?= ($car->transmission == "Manual") ? "selected" : null; ?>>Manual</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="total_passenger">Penumpang</label>
                        <input type="number" name="total_passenger" id="total_passenger" class="form-control" placeholder="Cth. 8" value="<?= $car->total_passenger ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="other_features">Fitur Tambahan</label>
                        <div class="ml-3">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="ac" <?= ($car->ac == "1") ? "checked" : null; ?>> AC </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="abs" <?= ($car->abs == "1") ? "checked" : null; ?>> ABS </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="day_running_light" <?= ($car->day_running_light == "1") ? "checked" : null; ?>> Day Running Light </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Deskripsi Tambahan</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="Masukkan keterangan"><?= $car->description ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">

                        <label for="picture">Foto sampul</label><br>

                        <?php if ($car->picture != null) : ?>
                            <figure class="figure figure-picture_car">
                                <img src="<?= base_url('assets/uploads/cars/cover/' . $car->picture) ?>" data-src="<?= $car->picture ?>" class="figure-img img-fluid rounded img-picture_car shadow-sm" alt=" ..." style="max-width: 180px;">
                                <figcaption class="figure-caption text-right">Foto sampul <br>
                                    <div class="btn btn-sm btn-danger mt-2 btn-delete-picture_car">Hapus</div>
                                </figcaption>
                            </figure>
                        <?php else : ?>
                            <figure class="figure figure-picture_car">
                                <img src="<?= base_url('assets/images/placeholder/no_img.png') ?>" data-src="<?= $car->picture ?>" class="figure-img img-fluid rounded img-picture_car shadow-sm" alt="..." style="max-width: 180px;">
                            </figure>
                        <?php endif ?>
                        <div class="input-group mb-3">
                            <div class="custom-file input-picture_car">
                                <input type="file" class="custom-file-input" name="picture" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">Pilih atau seret file . . . </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label for="picture">Foto lainnya</label><br>

                    <?php foreach ($pictures_car as $pc) : ?>
                        <figure class="figure figure-pictures_car">
                            <img src="<?= base_url('assets/uploads/cars/' . $pc->title) ?>" data-id="<?= $pc->id_picture ?>" data-src="<?= $pc->title ?>" class="figure-img img-fluid rounded shadow-sm img-pictures_car" alt=" ..." style="max-width: 180px;">
                            <figcaption class="figure-caption text-right">
                                <a href="<?= site_url('panel/car/delete_pictures_car/' . $pc->token_picture . '/' . urlencode(encrypt_url($car->id_car))) ?>" class="btn btn-sm btn-danger mt-2 btn-delete-pictures_car" onclick="return confirm('Hapus data?')">Hapus</a>
                            </figcaption>
                        </figure>
                    <?php endforeach ?>

                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="price">Harga per Hari</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Rp</span>
                            </div>
                            <input type="number" name="price" id="price" class="form-control" placeholder="Cth. 500000" value="<?= $car->price ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        const imgLocation = $('.img-picture_car').data('src');

        if (imgLocation != "") {
            $('.input-picture_car').addClass(' d-none');
        } else {
            // $('.input-picture_car').removeClass(' d-none');
        }

        $(".btn-delete-picture_car").click(function() {

            $.ajax({
                url: '<?php echo site_url('panel/car/delete_picture'); ?>',
                data: {
                    src: imgLocation
                },
                method: 'POST',
                // dataType: 'JSON',
                success: function(data) {
                    alert(data);
                    $('.img-picture_car').attr('src', '<?= base_url('assets/images/placeholder/no_img.png') ?>');
                    $('.btn-delete-picture_car').hide();
                    $('.input-picture_car').removeClass(' d-none');
                }
            });
        });

        // $(".btn-delete-pictures_car").click(function() {

        //     const idPicture = $(this).data('id');
        //     const picLocation = $('.img-pictures_car' + idPicture).data('src');

        //     $.ajax({
        //         url: '<?php echo site_url('panel/car/delete_pictures'); ?>',
        //         data: {
        //             src: picLocation,
        //             id_picture: idPicture
        //         },
        //         method: 'POST',
        //         // dataType: 'JSON',
        //         success: function(data) {
        //             alert(data);
        //             // $("#right_contan .img_slider_car p.thumbs a[href$='noimage.png']").hide()

        //             // $("#" + idPicture).hide();
        //             // $(".btn-delete-pictures_car").hide(idPicture);


        //         }
        //     });
        // });
    });
</script>