<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['view_page'] = 'dashboard/index';
        $data['title'] = 'Admin &mdash; Dashboard';

        $this->load->view('admin/layout/main', $data);
    }
}
