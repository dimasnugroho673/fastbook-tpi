<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registration extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view('user/choose_login');
	}

	public function customer()
	{
	}

	public function seller()
	{
		$this->load->view('user/registration_customer');
	}

	public function user()
	{
		$email = $this->db->get_where('user_registration_token', array(
			'email_user' => $this->input->get('email')
		))->row();

		$token = $this->db->get_where('user_registration_token', array(
			'user_reg_token' => $this->input->get('token')
		))->row();

		if ($this->input->get('email') == null || $this->input->get('token') == null) {
			// echo "HTTP/1.1 404 Not Found";
			header('This is not the page you are looking for', true, 404);
			include(FCPATH . 'application/views/404.php');
			exit();
		} else {
			if (!($email) || !($token)) {
				echo "Authentikasi gagal";
			} else {
				if (time() - $token->date_created < (60 * 60 * 24 * 3)) {
					$role_id = $this->db->get_where('user_registration_token', array(
						'user_reg_token' => $this->input->get('token')
					))->row()->role_id;

					$data['role'] = $this->db->get_where('user_role', array(
						'id_user_role' => $role_id
					))->row()->role_name;

					$data['view_page'] = 'auth/registration';
					$data['title'] = 'Register &mdash; ' . $data['role'];
					$data['roles'] = $this->db->get_where('user_role', array(
						'id_user_role' => $token->role_id
					))->result();

					$this->form_validation->set_rules('username', 'Fullname', 'trim|required');
					$this->form_validation->set_rules('email', 'Email', 'trim|required');
					$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password', 'trim|required|matches[password1]');

					if ($this->form_validation->run() == false) {
						$this->load->view('admin/auth/main', $data);
					} else {

						$dataInsert = array(
							'username' 	=> htmlspecialchars($this->input->post('username', TRUE)),
							'email'		=> htmlspecialchars($this->input->post('email', TRUE)),
							'password'	=> password_hash($this->input->post('password2', TRUE), PASSWORD_DEFAULT),
							'role_id'	=> htmlspecialchars($this->input->post('role_id', TRUE)),
							'no_hp'		=> htmlspecialchars($this->input->post('no_hp', TRUE)),
							'is_active'		=> 1,
							'remember_me' => null,
							'joined_at'		=> time(),
							'update_at'		=> null,
							'avatar'		=> 'default.jpg'
						);

						$this->db->insert('admin', $dataInsert);

						$this->db->delete('user_registration_token', array(
							'user_reg_token' => $this->input->get('token')
						));
						$this->session->set_flashdata('status_success_reg', "Registrasi berhasil, silahkan login untuk masuk ke sistem Fastbook");
						redirect('auth/master');
					}
				} else {
					$this->db->delete('user_registration_token', array(
						'email_user' => $this->input->get('email')
					));

					echo "Link expired!";
				}
			}
		}
	}


	private function _getToken()
	{
		$n = 20;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';

		for ($i = 0; $i < $n; $i++) {
			$index = rand(0, strlen($characters) - 1);
			$randomString .= $characters[$index];
		}

		return $randomString;
	}

	public function seller_action()
	{
		$name = $this->input->post("name_customer");
		$email = $this->input->post('email');
		$merchant_name = $this->input->post('merchant_name');
		$password = $this->input->post('password_customer');
		$no_hp = $this->input->post('no_hp');
		$reason_join = $this->input->post('reason_join');


		$data = array(
			'name_customer' => $name,
			'customer_type' => 'seller',
			'email' 		=> $email,
			'no_hp'			=> $no_hp,
			'account_type'	=> 'trial',
			'merchant_name' => $merchant_name,
			'password_customer'		=> password_hash($password, PASSWORD_DEFAULT),
			'first_join'	=> time(),
			'is_trial'		=> 1,
			'trial_end'		=> time() + (60 * 60 * 24 * 30 * 6),
			'is_active'		=> 1,
			'is_verify'		=> 0,
			'reason_join'	=> $reason_join
		);

		$this->db->insert('customer', $data);
		$this->session->set_flashdata('message', 'Anda berhasil mendaftar, silahkan masuk');
		redirect('registration/seller');
	}
}
