<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Discount</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Discount List</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>


        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-discount" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Berhasil menambahkan kode diskon baru
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-delete-discount" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} seconds ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Kode diskon <strong><?= $this->session->flashdata('discount_code'); ?></strong> Telah dihapus
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-edit-discount" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Berhasil mengupdate kode diskon
                </div>
            </div>
        </div>



        <!-- Button trigger modal -->
        <div class="row">
            <div class="col-md-8">
                <div class="mb-4 text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddDiscountCode">
                        Tambah kode
                    </button>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-8">
                <table class="table table-hover table-striped table-sm" id="dataDiscount">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Aksi</th>
                            <th data-orderable="false">Kode diskon</th>
                            <th>Potongan harga</th>
                            <th data-orderable="false">Tanggal mulai</th>
                            <th data-orderable="false">Tanggal selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($discounts as $d) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-outline-primary dropdown-toggle" href="javascript:;" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item btn-delete-discount_code" href="<?= site_url('admin/discount/delete/' . urlencode(encrypt_url($d->id_discount_code))) ?>"><i class="feather icon-trash-2 mr-1"></i> Hapus</a>
                                            <a class="dropdown-item btn-edit-discount_code" href="javascript:;" data-id="<?= urlencode(encrypt_url($d->id_discount_code)) ?>"><i class="feather icon-edit-2 mr-1"></i> Edit</a>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="text-primary"><?= $d->discount_code ?></span></td>
                                <td><?= $d->discount_percent * 100 ?>%</td>
                                <td><?= date('d F Y', $d->date_start)  ?></td>
                                <td><?= date('d F Y', $d->date_end) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th></th>
                            <th>Kode diskon</th>
                            <th>Potongan harga</th>
                            <th>Tanggal mulai</th>
                            <th>Tanggal selesai</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>


    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modalAddDiscountCode" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalAddDiscountCodeLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/discount') ?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAddDiscountCodeLabel">Tambah kode</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="discount_code">Kode diskon</label>
                        <input type="text" name="discount_code" id="discount_code" class="form-control" placeholder="Cth. NEWNORMAL20">
                    </div>
                    <div class="form-group">
                        <label for="discount_percent">Potongan harga</label>
                        <div class="input-group mb-3">
                            <input type="text" name="discount_percent" id="discount_percent" class="form-control col-2" placeholder="Cth. 20">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_start">Tanggal mulai berlaku</label>
                        <input type="date" name="date_start" id="date_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="date_end">Tanggal selesai berlaku</label>
                        <input type="date" name="date_end" id="date_end" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEditDiscountCode" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalEditDiscountCodeLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/discount/update') ?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditDiscountCodeLabel">Edit kode diskon</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_discount_code" id="id_discount_code_edit" class="form-control">
                    <div class="form-group">
                        <label for="discount_code">Kode diskon</label>
                        <input type="text" name="discount_code" id="discount_code_edit" class="form-control" placeholder="Cth. NEWNORMAL20">
                    </div>
                    <div class="form-group">
                        <label for="discount_percent">Potongan harga</label>
                        <div class="input-group mb-3">
                            <input type="text" name="discount_percent" id="discount_percent_edit" class="form-control col-2" placeholder="Cth. 20">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_start">Tanggal mulai berlaku</label>
                        <input type="date" name="date_start" id="date_start_edit" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="date_end">Tanggal selesai berlaku</label>
                        <input type="date" name="date_end" id="date_end_edit" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataDiscount').dataTable();

        $('.btn-edit-discount_code').on('click', function() {
            $('#modalEditDiscountCode').modal('show');

            const id = $(this).data('id');

            function convert(date_event) {

                // Months array
                var months_arr = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

                // Convert timestamp to milliseconds
                var date = new Date(date_event * 1000);

                // Year
                var year = date.getFullYear();

                // Month
                var month = months_arr[date.getMonth()];

                // Day
                var day = date.getDate();

                // Display date time in MM-dd-yyyy 
                return convdataTime = year + '-' + month + '-' + day;

            }

            $.ajax({
                url: '<?= site_url('admin/discount/get_data_discount'); ?>',
                data: {
                    id: id
                },
                method: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $('#id_discount_code_edit').val(data.id_discount_code);
                    $('#discount_code_edit').val(data.discount_code);
                    $('#discount_percent_edit').val(data.discount_percent * 100);
                    $('#date_start_edit').val(convert(data.date_start));
                    $('#date_end_edit').val(convert(data.date_end));
                }
            });


        });
    });
</script>