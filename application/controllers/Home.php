<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_car');
    }

    public function index()
    {
        $data['cars'] = $this->M_car->get_cars()->result();

        $this->load->view('user/home/index', $data);
    }

    public function show($id_car)
    {
        $data['car'] = $this->M_car->get_car($id_car)->row();

        $this->load->view('user/home/detail_car', $data);
    }
}
