Mobil <?= $car->name_car ?> by <?= $car->name_customer ?>
<br><br>
jumlah yang harus dibayar : <?= "Rp " . number_format($price_to_booking, 2, ',', '.'); ?>
<br><br>

<form action="<?= site_url('booking/n/') ?>" method="post">
    <input type="date" name="date_booking">
    <br>
    <br>
    <input type="date" name="until_date_booking">
    <br>
    <br>
    <input type="text" name="price_payment" value="<?= $price_to_booking ?>" readonly>
    <input type="hidden" name="id_car" value="<?= urlencode(encrypt_url($car->id_car)); ?>" readonly>
    <br>
    <select name="transferbank" id="">
        <option value="bri">bri</option>
        <option value="bna">bna</option>
        <option value="bca">bca</option>
    </select>
    <br>
    <button type="submit">Booking now</button>
</form>