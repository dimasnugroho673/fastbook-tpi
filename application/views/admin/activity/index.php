<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Activity</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Activity</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-unblock-customer" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah dicabut status blokir
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-block-customer" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} seconds ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah diblokir
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-stripped table-hover table-sm" id="dataCustomers">
                    <thead>
                        <tr>
                            <th data-orderable="false">No</th>
                            <th data-orderable="false">Aksi</th>
                            <th>Nama</th>
                            <th>Tipe</th>
                            <th data-orderable="false">Toko mobil</th>
                            <th>Membership</th>
                            <th>Join at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;

                        foreach ($customers as $c) :  ?>
                            <tr>
                                <th><?= $no++; ?></th>
                                <td>
                                    <a href="<?= site_url('admin/activity/detail/' . urlencode(encrypt_url($c->id_customer))) ?>" class="btn btn-sm btn-outline-primary">Pantau</a>
                                </td>
                                <td><strong><?= $c->name_customer ?> <?php echo ($c->is_active == 99) ? '<span class="badge badge-light-danger ml-1">Diblokir admin</span>' : null; ?> </strong><br>
                                    <small>@<?= $c->username ?> &mdash; <?= $c->email ?></small>
                                </td>
                                <td><?= $c->customer_type ?></td>
                                <td><?= $c->merchant_name ?></td>
                                <td><?php if ($c->account_type == 'trial') { ?>
                                        <span class="badge badge-light-secondary">Trial<i class="feather icon-clock ml-1"></i></span><br>
                                        <small>Trial end at <?= date('d F Y', $c->trial_end) ?></small>
                                    <?php } else { ?>
                                        <span class="badge badge-light-warning">Premium<i class="feather icon-gitlab ml-1"></i></span>
                                    <?php } ?>
                                </td>
                                <td><?= date('d F Y', $c->first_join) ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/customers/block/' . urlencode(encrypt_url($c->id_customer))) ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Kirimkan pesan</h5>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="message">Pesan</label>
                        <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Tambahkan pesan"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Understood</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#dataCustomers').dataTable({
            "columnDefs": [{
                "orderable": false,
                "targets": 1
            }],
        });

    });
</script>