<style>
    .auth-content {
        /* min-width: 900px; */
    }
</style>

<div class="auth-wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center text-center">
                <div class="col-md-12">
                    <div class="card-body">
                        <img src="assets/images/logo-dark.png" alt="" class="img-fluid mb-4">
                        <h4 class="mb-3 f-w-400">User Registration</h4>
                        <form action="<?= site_url('registration/user?email=' . $this->input->get('email') . '&token=' . urlencode($this->input->get('token'))) ?>" method="post">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control rounded-right <?php echo (form_error('username')) ? "is-invalid" : null; ?>" name="username" placeholder="Full name" value="<?= set_value('username') ?>">
                                        <div class="invalid-feedback">
                                            <?= form_error('username') ?>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                        </div>
                                        <input type="email" name="email" class="form-control rounded-right <?php echo (form_error('email')) ? "is-invalid" : null; ?>" placeholder="Email address" value="<?= $this->input->get('email') ?>" value="<?= set_value('email') ?>">
                                        <div class="invalid-feedback">
                                            <?= form_error('email') ?>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                        </div>
                                        <input type="password" name="password1" class="form-control rounded-right <?php echo (form_error('password1')) ? "is-invalid" : null; ?>" placeholder="Password">
                                        <div class="invalid-feedback">
                                            <?= form_error('password1') ?>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                        </div>
                                        <input type="password" name="password2" class="form-control rounded-right <?php echo (form_error('password2')) ? "is-invalid" : null; ?>" placeholder="Retype password">
                                        <div class="invalid-feedback">
                                            <?= form_error('password2') ?>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-phone"></i></span>
                                        </div>
                                        <input type="text" name="no_hp" class="form-control rounded-right" placeholder="No HP" value="<?= set_value('no_hp') ?>">
                                    </div>

                                    <label class="text-muted">Mendaftar sebagai</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">Role</label>
                                        </div>
                                        <select name="role_id" id="role_id" class="form-control" readonly>
                                            <?php foreach ($roles as $r) : ?>
                                                <option value="<?= $r->id_user_role ?>" selected><?= $r->role_name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                            </div>



                            <div class="form-group text-left mt-2">
                                <div class="checkbox checkbox-primary d-inline">
                                    <input type="checkbox" name="checkbox-fill-2" id="checkbox-fill-2">
                                    <label for="checkbox-fill-2" class="cr">Saya mengerti dan menyetujui <a href="#!"> Aturan</a> yang berlaku.</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block mb-4">Sign up</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>