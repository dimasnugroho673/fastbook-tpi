<?php if ($this->session->flashdata('message')) : ?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url('assets') ?>/bootstrap4.5.0/css/bootstrap.css">

        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,400&display=swap" rel="stylesheet">

        <!-- icons -->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">

        <link rel="stylesheet" href="<?= base_url('assets') ?>/userpanel/assets/css/style.css">

        <!-- datatables -->
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/datatables/dataTables.bootstrap4.min.css">

        <!-- jquery -->
        <script src="<?= base_url('assets') ?>/jquery/jquery-3.5.1.min.js"></script>

        <title>Fastbook</title>

        <style>
            * {
                font-family: 'Open Sans', sans-serif !important;
            }

            .nav-link {
                font-weight: 700;
            }

            .nav-link,
            .font-14 {
                font-size: 14px !important;
            }

            .navbar-nav .active {
                font-weight: 600;
                color: #D33036 !important;
            }

            .color-primary {
                background-color: #D33036 !important;
            }

            .btn-login {
                background-color: #D33036 !important;
                font-weight: 600 !important;
            }

            .jumbotron {
                height: 400px !important;
            }

            .list-group .active {
                font-weight: 700;
            }

            .breadcrumb .breadcrumb-item {
                font-size: 13px !important;

            }

            .img-success {
                margin-top: 140px !important;
            }
        </style>
    </head>

    <body>

        <div class="container">


            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="<?= base_url('assets/images/ilustration/success_token_confirm.png') ?>" class=" mx-auto img-success" alt="Responsive image" style="max-width: 400px; display:inline-block;">
                    <h4 style="font-weight: 800;"><?= $this->session->flashdata('message') ?></h4>
                    <a href="<?= site_url('panel/membership') ?>" class="btn btn-danger mt-3" style="min-width: 180px; letter-spacing:2px">Kembali</a>
                </div>
            </div>
        </div>


        <script src="<?php echo base_url('assets'); ?>/bootstrap4.5.0/js/popper.min.js">
        </script>
        <script src="<?php echo base_url('assets'); ?>/bootstrap4.5.0/js/bootstrap.min.js">
        </script>

        <!-- datatables -->
        <script src="<?php echo base_url('assets'); ?>/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/datatables/dataTables.bootstrap4.min.js"></script>

        <script>
            //Main navigation scroll spy for shadow
            $(window).scroll(function() {
                var y = $(window).scrollTop();
                if (y > 0) {
                    $("#navbar").addClass('border-bottom');
                } else {
                    $("#navbar").removeClass('border-bottom');
                }
            });
        </script>
    </body>

    </html>
<?php else : ?>
    <?php redirect('panel/membership') ?>
<?php endif ?>