<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Discount extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_discount');
    }

    public function index()
    {
        $data['view_page'] = 'discount/index';
        $data['title'] = 'Admin &mdash; Discount';

        $data['discounts'] = $this->M_discount->adminDiscountListing()->result();

        $this->form_validation->set_rules('discount_code', 'Kode diskon', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/layout/main', $data);
        } else {
            $get_discount_percent = htmlspecialchars($this->input->post('discount_percent', TRUE));
            $discount_percent = $get_discount_percent * 1 / 100;

            $dataInsert = array(
                'discount_code'     => strtoupper(htmlspecialchars($this->input->post('discount_code', TRUE))),
                'discount_percent'  => $discount_percent,
                'date_start'        => strtotime(htmlspecialchars($this->input->post('date_start', TRUE))),
                'date_end'          => strtotime(htmlspecialchars($this->input->post('date_end', TRUE))),
                'date_created'      => time()
            );

            $this->db->insert('discount_code', $dataInsert);

            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-add-discount').toast('show');
            });
            </script>");
            redirect('admin/discount');
        }

        // $this->load->view('admin/layout/main', $data);
    }

    public function update()
    {
        $get_id_discount_code = $this->input->post('id_discount_code');

        $get_discount_form_db = $this->db->get_where('discount_code', array(
            'id_discount_code' => $get_id_discount_code
        ))->row();

        $get_discount_percent = htmlspecialchars($this->input->post('discount_percent', TRUE));
        $discount_percent = $get_discount_percent * 1 / 100;

        $dataUpdate = array(
            'discount_code'     => strtoupper(htmlspecialchars($this->input->post('discount_code', TRUE))),
            'discount_percent'  => $discount_percent,
            'date_start'        => strtotime(htmlspecialchars($this->input->post('date_start', TRUE))),
            'date_end'          => strtotime(htmlspecialchars($this->input->post('date_end', TRUE))),
            'date_created'      => time()
        );

        $this->db->where('id_discount_code', $get_id_discount_code);
        $this->db->update('discount_code', $dataUpdate);
        $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-edit-discount').toast('show');
            });
            </script>");
        redirect('admin/discount');
    }

    public function delete($id_discount_code)
    {
        $get_id_discount_code = urldecode(decrypt_url($id_discount_code));

        $get_discount_form_db = $this->db->get_where('discount_code', array(
            'id_discount_code' => $get_id_discount_code
        ))->row();

        $this->db->delete('discount_code', array(
            'id_discount_code' => $get_id_discount_code
        ));

        $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-delete-discount').toast('show');
            });
            </script>");
        $this->session->set_flashdata('discount_code', $get_discount_form_db->discount_code);
        redirect('admin/discount');
    }

    // AJAX REQUEST
    public function get_data_discount()
    {
        $get_id_discount_code = urldecode(decrypt_url($this->input->post('id')));
        $dataSend = $this->db->get_where('discount_code', array('id_discount_code' => $get_id_discount_code))->row();
        $dataId = urlencode(encrypt_url($dataSend->id_discount_code));

        // $sendingData = [
        //     ''
        // ]

        echo json_encode($dataSend);
    }
}
