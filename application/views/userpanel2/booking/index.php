<div class="card">
    <div class="card-header">
        <h4>Daftar Booking</h4>
    </div>
    <div class="card-body">


        <div class="my-3">
            <?= $this->session->flashdata('status') ?>
        </div>

        <div class="table-reponsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Aksi</th>
                        <th>Customer</th>
                        <th>Mobil - Plat</th>
                        <th>Tanggal Booking - Selesai</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($bookings)) { ?>
                        <?php $no = 1;
                        foreach ($bookings as $b) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <small><a class="dropdown-item" href="#">Konfirmasi pembayaran</a></small>
                                            <small><a class="dropdown-item" href="#">Another action</a></small>
                                            <small><a class="dropdown-item" href="#">Something else here</a></small>
                                        </div>
                                    </div>
                                </td>
                                <td><?= $b->id_customer ?></td>
                                <td><?= $b->name_car ?> &mdash; <?= $b->plat_number ?></td>
                                <td><?= date('d F Y', $b->date_booking) ?> &mdash; <?= date('d F Y', $b->until_date_booking) ?></td>
                                <td><?= date('d F Y H:i:s', $b->limit_date_payment)  ?></td>
                                <td><?php
                                    if ($b->status == 'waiting payment') {
                                        echo '<span class="badge badge-warning">Menunggu pembayaran</span>';
                                    } elseif ($b->status == 'success payment') {
                                        echo '<span class="badge badge-success">Pembayaran sukses</span>';
                                    } ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>
            <?php if (empty($bookings)) { ?>
                <div class="alert alert-danger text-center" role="alert">
                    Belum ada booking dilakukan
                </div>
            <?php } ?>
        </div>

    </div>
</div>