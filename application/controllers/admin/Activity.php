<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Activity extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_customer');
        $this->load->model('M_booking');
    }

    public function index()
    {
        $data['view_page'] = 'activity/index';
        $data['title'] = 'Admin &mdash; Activity';

        $data['customers'] = $this->M_customer->adminMemberListing()->result();

        $this->load->view('admin/layout/main', $data);
    }

    public function detail($id_customer)
    {

        $get_id_customer = urldecode(decrypt_url($id_customer));

        $get_data_customer = $this->db->get_where('customer', array(
            'id_customer' => $get_id_customer
        ))->row();

        $data['title'] = 'Admin &mdash; Detail | ' . $get_data_customer->name_customer;

        if ($get_data_customer->customer_type == 'customer') {
            $data['view_page'] = 'activity/detail_customer';

            $data['bookings'] = $this->M_booking->adminCustomerBookingMonitor($get_data_customer->id_customer)->result();

            $this->load->view('admin/layout/main', $data);
        } else {
            $data['view_page'] = 'activity/detail_seller';

            $data['bookings'] = $this->M_booking->adminSellerBookingMonitor($get_data_customer->id_customer)->result();
            $data['payments'] = $this->M_booking->adminSellerPaymentMonitor($get_data_customer->id_customer)->result();

            $this->load->view('admin/layout/main', $data);
        }
    }
}
