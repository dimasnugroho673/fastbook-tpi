<?php

class M_user extends CI_Model
{
    public function adminUserListing()
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->join('user_role', 'user_role.id_user_role = admin.role_id');
        return $this->db->get();
    }
}
