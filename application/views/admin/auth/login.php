<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center text-center">
                <div class="col-md-12">
                    <div class="card-body">
                        <form action="<?= site_url('auth/master') ?>" method="post">
                            <!-- <img src="<?= base_url('assets/admin/dist/'); ?>assets/images/logo-dark.png" alt="" class="img-fluid mb-4"> -->
                            <!-- <span><?= $captcha_image ?></span> -->
                            <h4 class="mb-3 f-w-400">Admin authentication</h4>

                            <?php if ($this->session->flashdata('status_success_reg')) { ?>
                                <div class="alert alert-success" role="alert">
                                    <i class="feather icon-check mr-1"></i> <?= $this->session->flashdata('status_success_reg') ?>
                                </div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('status_success_reset_password')) { ?>
                                <div class="alert alert-success" role="alert">
                                    <i class="feather icon-check mr-1"></i> <?= $this->session->flashdata('status_success_reset_password') ?>
                                </div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('message')) { ?>
                                <?= $this->session->flashdata('message') ?>
                            <?php } ?>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                </div>
                                <input type="text" name="email" class="form-control rounded-right <?php echo (form_error('email')) ? "is-invalid" : null; ?>" placeholder="Email address">
                                <div class="invalid-feedback">
                                    <?= form_error('email') ?>
                                </div>
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control rounded-right <?php echo (form_error('password')) ? "is-invalid" : null; ?>" placeholder="Password">
                                <div class="invalid-feedback">
                                    <?= form_error('email') ?>
                                </div>
                            </div>
                            <div class="form-group text-left mt-2">
                                <div class="checkbox checkbox-primary d-inline">
                                    <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" checked="">
                                    <label for="checkbox-fill-a1" class="cr"> Save credentials</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary mb-4">Login</button>
                            <p class="mb-1 text-muted">Forgot password? <a href="<?= site_url('auth/master/forgot') ?>" class="f-w-400">Reset</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ auth-signin ] end -->