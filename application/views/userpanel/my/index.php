<div class="jumbotron alert bg-warning shadow-sm"">
    <p>Anda belum melakukan verifikasi email. Silahkan melakukan verifikasi email agar anda dapat menggunakan fitur dalam aplikasi lebih lengkap!</p>
    <hr class=" my-4">
    <a class="btn btn-primary btn-sm" href="#" role="button">Verifikasi sekarang</a>
</div>