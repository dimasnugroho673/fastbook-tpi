<?php

$count_unread_transactions = $this->db->get_where('invoice', ['is_new_invoice' => 1, 'for_seller' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer]);

$count_unread_booking = $this->db->get_where('booking', ['is_new_booking' => 1, 'id_seller' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer]);
?>

<!-- Start team Area -->
<section class="team-area section-gap team-page-teams" id="team">
    <div class="batas" style="margin-left:50px; margin-right:50px">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a class="list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'my') ? 'active' : null; ?>" href="<?= site_url('panel/my') ?>">Profile</a>
                    <a class="list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'store') ? 'active' : null; ?>" href="<?= site_url('panel/store') ?>">Toko Saya</a>
                    <a class="list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'car') ? 'active' : null; ?>" href="<?= site_url('panel/car') ?>">Mobil</a>
                    <a class="list-group-item list-group-item-action" href="#list-item-4">Angkutan barang</a>
                    <a class="list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'transaction') ? 'active' : null; ?>" href="<?= site_url('panel/transaction') ?>">Transaksi
                        <?php if ($count_unread_transactions->num_rows() > 0) { ?>
                            <span class="badge badge-light"><?= $count_unread_transactions->num_rows() ?></span>
                        <?php } ?>
                    </a>
                    <a class="list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'booking') ? 'active' : null; ?>" href="<?= site_url('panel/booking') ?>">Booking
                        <?php if ($count_unread_booking->num_rows() > 0) { ?>
                            <span class="badge badge-light"><?= $count_unread_booking->num_rows() ?></span>
                        <?php } ?>
                    </a>
                    <a class=" list-group-item list-group-item-action <?php echo ($this->uri->segment(2) == 'setting') ? 'active' : null; ?>" href="#list-item-5">Pegaturan</a>
                </div>
            </div>
            <div class="col-md-9">
                <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
                    <?php $this->load->view('userpanel/' . $view_page); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End team Area -->