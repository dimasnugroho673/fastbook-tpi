<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Membership extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();


        $data['my_token'] = $this->db->get_where('token_customer', array('customer_using' => $data['check_user']->id_customer))->result();
        $data['view_page'] = 'membership/index';

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/layout/sidebar', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }

    public function confirmtoken()
    {
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();


        $data['get_data_token'] = $this->db->get_where('token_customer', array('stock_token' => $this->input->post('token_code')))->row();
        $data['view_page'] = 'membership/confirm_token';
        $data['token'] = htmlspecialchars($this->input->post('token_code', TRUE));

        $this->form_validation->set_rules('token_confirm', 'Token', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('userpanel/layout/header', $data);
            $this->load->view('userpanel/layout/sidebar', $data);
            $this->load->view('userpanel/layout/footer', $data);
        } else {
            // 
            $tokenUpdate = array(
                'status'            => 'active',
                'customer_using'    => $data['check_user']->id_customer,
                'date_activation'   => time()
            );

            $customerUpdate = array(
                'account_type'          => 'premium',
                'token'                 => htmlspecialchars($this->input->post('token_confirm', TRUE)),
                'token_active_start'    => time(),
                'token_active_end'      => htmlspecialchars($this->input->post('token_active_end', TRUE)),
                'is_trial'              => 0,
            );

            $this->db->where('stock_token', $this->input->post('token_confirm', TRUE));
            $this->db->update('token_customer', $tokenUpdate);

            $this->db->where('id_customer', $data['check_user']->id_customer);
            $this->db->update('customer', $customerUpdate);

            $this->session->set_flashdata('message', 'Selamat akun kamu telah aktif menjadi premium');
            redirect('panel/membership/success');
        }
    }

    public function success()
    {

        $this->load->view('userpanel/membership/success_confirm');
    }
}
