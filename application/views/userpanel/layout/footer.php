<!-- ================== FOOTER =================== -->




<footer class="page-footer font-small mt-5 font-14 bg-white" style="border: 1px solid rgba(0, 0, 0, .1);">

	<div class="bg-light">
		<div class="container">
			<div class="row py-4 d-flex align-items-center">
				<div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
					<h6 class="mb-0 font-14">Get connected with us on social networks!</h6>
				</div>
				<!-- icons -->
				<div class="col-md-6 col-lg-7 text-center text-md-right icon">
					<!-- Facebook -->
					<a class="fb-ic sosmed">
						<i class="fab fa-facebook-f mr-4"> </i>
					</a>
					<!-- Twitter -->
					<a class="tw-ic sosmed">
						<i class="fab fa-twitter mr-4"> </i>
					</a>
					<!-- Google +-->
					<a class="gplus-ic sosmed">
						<i class="fab fa-google-plus-g mr-4"> </i>
					</a>
					<!--Linkedin -->
					<a class="li-ic sosmed">
						<i class="fab fa-linkedin-in mr-4"> </i>
					</a>
					<!--Instagram-->
					<a class="ins-ic sosmed">
						<i class="fab fa-instagram "> </i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer Links -->
	<div class="container text-center text-md-left mt-5 mb-4">
		<div class=" row mt-3">
			<div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

				<h6 class="text-uppercase font-weight-bold mb-4">Fastbook</h6>
				<p class="text-muted">Retrohub is an ecommerce website that sells a variety of classic items
					that
					are vintage and retro
				</p>

			</div>

			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
				<h6 class="text-uppercase font-weight-bold mb-4">Products</h6>
				<p><a class="text-muted" href="#!">How Shopping</a></p>
				<p><a class="text-muted" href="#!">Payment</a></p>
				<p><a class="text-muted" href="#!">Blog</a></p>
				<p><a class="text-muted" href="#!">Return Payment</a></p>
			</div>

			<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
				<h6 class="text-uppercase font-weight-bold mb-4">Contact</h6>
				<p class="text-muted"><i class="fas fa-building mr-3"></i> Jl. Gurindam 12 Tanjungpinang Kota,
					Kepulauan Riau</p>
				<p class="text-muted"><i class="fas fa-envelope mr-3"></i> admin@computer-cyber.com</p>
				<p class="text-muted"><i class="fas fa-phone mr-3"></i> + 0771 2346 788</p>
				<p class="text-muted"><i class="fas fa-print mr-3"></i> + 0771 2346 788</p>
			</div>

		</div>
	</div>

	<!-- Copyright -->
	<div class="footer-copyright py-3">
		<div class="container text-center mb-4"><small>© 2020 Copyright <span class="retrohub">Fastbook </span>
				Powered
				by <span class="eliza">Eliza Team</span></small>
		</div>
	</div>
	<!-- Copyright -->

</footer>


<script>
	$(document).ready(function() {
		$('.custom-file-input').on('change', function() {
			let fileName = $(this).val().split('\\').pop();
			$(this).next('.custom-file-label').addClass("selected").html(fileName);
		});
	});
</script>

<!-- ================== END FOOTER =================== -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="<?php echo base_url('assets'); ?>/bootstrap4.5.0/js/popper.min.js">
</script>
<script src="<?php echo base_url('assets'); ?>/bootstrap4.5.0/js/bootstrap.min.js">
</script>

<!-- datatables -->
<script src="<?php echo base_url('assets'); ?>/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/datatables/dataTables.bootstrap4.min.js"></script>

<script>
	//Main navigation scroll spy for shadow
	$(window).scroll(function() {
		var y = $(window).scrollTop();
		if (y > 0) {
			$("#navbar").addClass('border-bottom');
		} else {
			$("#navbar").removeClass('border-bottom');
		}
	});
</script>
</body>

</html>