<?php

class M_transaction extends CI_Model
{
    public function seller_listing($check_user)
    {
        $this->db->select('*');
        $this->db->from('invoice');
        $this->db->join('customer', 'invoice.from_customer = customer.id_customer');
        $this->db->join('booking', 'booking.id_invoice = invoice.id_invoice');
        $this->db->where('for_seller', $check_user);
        return $this->db->get();
    }
}
