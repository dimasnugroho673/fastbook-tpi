<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class Customers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_customer');
    }

    public function index()
    {
        $data['view_page'] = 'customer/index';
        $data['title'] = 'Admin &mdash; Customers';

        $data['customers'] = $this->M_customer->adminMemberListing()->result();

        $this->load->view('admin/layout/main', $data);
    }

    public function block()
    {
        $get_id_customer = urldecode(decrypt_url($this->input->post('id_customer')));

        $get_data_customer = $this->db->get_where('customer', array(
            'id_customer' => $get_id_customer
        ))->row();

        $this->db->set('is_active', 99);
        $this->db->where('id_customer', $get_id_customer);
        $this->db->update('customer');

        if ($this->input->post('message') != null) {
            $message = $this->input->post('message') . '. Hubungi <a href="#">admin.fastbook@gmail.com</a> untuk melakukan pembelaan';
        } else {
            $message = 'Akun anda telah diblokir karena melakukan kesalahan atau pelanggaran. Hubungi <a href="#">admin.fastbook@gmail.com</a> untuk melakukan pembelaan';
        }

        $this->_sendMail($get_data_customer->email, $get_data_customer->name_customer, "Pemblokiran akun oleh admin", $message);

        $this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-block-customer').toast('show');
		});
        </script>");
        $this->session->set_flashdata('name_customer', $get_data_customer->name_customer);
        redirect('admin/customers');
    }

    public function unblock($id_customer)
    {
        $get_id_customer = urldecode(decrypt_url($id_customer));

        $get_data_customer = $this->db->get_where('customer', array(
            'id_customer' => $get_id_customer
        ))->row();

        $this->db->set('is_active', 1);
        $this->db->where('id_customer', $get_id_customer);
        $this->db->update('customer');

        $message = 'Selamat akun anda telah kembali aktif, jan bandel ya.';
        $this->_sendMail($get_data_customer->email, $get_data_customer->name_customer, "Pencabutan blokir akun", $message);

        $this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-unblock-customer').toast('show');
		});
        </script>");
        $this->session->set_flashdata('name_customer', $get_data_customer->name_customer);
        redirect('admin/customers');
    }

    private function _sendMail($email, $name, $subject, $message)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.computer-cyber.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'admin@computer-cyber.org';                     // SMTP username
            $mail->Password   = 'computercyber2018';                               // SMTP password
            $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('admin@computer-cyber.org', 'Fastbook');
            $mail->addAddress($email, $name);
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
