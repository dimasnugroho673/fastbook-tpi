<?php

$count_unread_transactions = $this->db->get_where('invoice', ['is_new_invoice' => 1, 'for_seller' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer]);

$count_unread_booking = $this->db->get_where('booking', ['is_new_booking' => 1, 'id_seller' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer]);
?>

<style>
    .list-group .active {
        background-color: #D33036;
        border-color: #D33036;
    }
</style>

<div class="batas font-14" style="margin-left:50px; margin-right:50px">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a class="list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'my') ? 'active' : null; ?>" href="<?= site_url('panel/my') ?>" href=""><i class="fas fa-user-circle mr-2"></i> Profile</a>
                <a class="list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'store') ? 'active' : null; ?>" href="<?= site_url('panel/store') ?>"><i class="fas fa-store mr-2"></i> Toko Saya</a>
                <a class="list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'car') ? 'active' : null; ?>" href="<?= site_url('panel/car') ?>"><i class="fas fa-car mr-2"></i>
                    Mobil</a>
                <a class="list-group-item list-group-item-action p-3" href="#list-item-4"><i class="fas fa-truck-pickup mr-2"></i> Angkutan barang</a>
                <a class="list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'transaction') ? 'active' : null; ?>" href="<?= site_url('panel/transaction') ?>"><i class="fas fa-money-check-alt mr-2"></i> Transaksi
                    <?php if ($count_unread_transactions->num_rows() > 0) { ?>
                        <span class="badge badge-primary badge-pill"><?= $count_unread_transactions->num_rows() ?></span>
                    <?php } ?>
                </a>
                <a class="list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'booking') ? 'active' : null; ?>" href="<?= site_url('panel/booking') ?>"><i class="fas fa-bookmark mr-2"></i>
                    Booking
                    <?php if ($count_unread_booking->num_rows() > 0) { ?>
                        <span class="badge badge-primary badge-pill"><?= $count_unread_booking->num_rows() ?></span>
                    <?php } ?>
                </a>
                <a class="list-group-item list-group-item-action p-3" href="<?= site_url('panel/membership') ?>"><i class="fas fa-crown mr-2"></i>
                    Membership
                </a>

                <a class=" list-group-item list-group-item-action p-3 <?php echo ($this->uri->segment(2) == 'setting') ? 'active' : null; ?>" href="#list-item-5"><i class="fas fa-cog mr-2"></i> Pengaturan</a>
            </div>
        </div>
        <div class="col-md-9">
            <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
                <?php $this->load->view('userpanel/' . $view_page); ?>
            </div>
        </div>
    </div>
</div>