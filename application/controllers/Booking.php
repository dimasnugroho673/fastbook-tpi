<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_car');

		// if ($this->db->get_where('customer', array('email' => $this->session->userdata('email'))->row()->customer_type != 'customer') {
		// 	$customer_type = true;
		// }

		// $this->session->set_userdata(array(
		// 	'id_session_for_guest' 	=> $this->_get_rand_sesssion(),
		// 	'unique_price'			=> $this->_get_unique_price()
		// ));
	}

	private function _get_unique_price()
	{
		$n = 3;
		$characters = '0123456789';
		$randomString = '';

		for ($i = 0; $i < $n; $i++) {
			$index = rand(0, strlen($characters) - 1);
			$randomString .= $characters[$index];
		}

		return $randomString;
	}

	private function _get_rand_sesssion()
	{
		$n = 4;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';

		for ($i = 0; $i < $n; $i++) {
			$index = rand(0, strlen($characters) - 1);
			$randomString .= $characters[$index];
		}

		return $randomString;
	}

	private function _get_invoice()
	{
		$n = 5;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';

		for ($i = 0; $i < $n; $i++) {
			$index = rand(0, strlen($characters) - 1);
			$randomString .= $characters[$index];
		}

		return $randomString;
	}

	public function r($id_car)
	{

		$get_id_car = urldecode(decrypt_url($id_car));

		$data['car'] = $this->M_car->get_car($get_id_car)->row();

		$data['price_to_booking'] = $data['car']->price / 2 + $this->_get_unique_price();

		$this->load->view('user/booking/index', $data);
	}

	public function n()
	{
		$get_id_car = urldecode(decrypt_url($this->input->post('id_car')));

		$get_booking_date = strtotime(date($this->input->post('date_booking')));
		$get_until_booking_date = strtotime(date($this->input->post('until_date_booking')));
		// echo date('d F Y', strtotime(date($this->input->post('date_booking'))));

		$check_date_booking_on_table_booking = $this->db->get_where('booking', array(
			'id_car'		=> $get_id_car,
			'date_booking' 	=> $get_booking_date,
			'until_date_booking' => $get_until_booking_date,
			'status'		=> 'on booked',
			'date_booking_update' => time(),
		))->row();

		if (!empty($check_date_booking_on_table_booking)) {
			echo "mobil sudah dibooking";
		} else {
			$invoice = array(
				'id_invoice'			=> 'TNJ' . $this->_get_invoice(),
				'from_customer'			=> $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer,
				'for_seller'    		=> $this->db->get_where('cars', array('id_car' => $get_id_car))->row()->id_user,
				'date_want_booking'		=> $get_booking_date,
				'limit_date_payment' 	=> time() + (60 * 60 * 24),
				'transfer_bank'			=> $this->input->post('transferbank'),
				'status_payment'		=> 'waiting',
				'is_new_invoice'		=> 1,
				'date_created'        	=> time()
			);

			$this->db->insert('invoice', $invoice);

			// print_r($this->db->get_where('cars', array('id_car' => $get_id_car))->row()->id_user);
			// die;

			$booking = array(
				'id_invoice' 			=> $this->db->get_where('invoice', array(
					'from_customer'			=> $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer,
					'for_seller'    		=> $this->db->get_where('cars', array('id_car' => $get_id_car))->row()->id_user,
					'date_want_booking'		=> $get_booking_date,
					'limit_date_payment' 	=> time() + (60 * 60 * 24),
					'transfer_bank'			=> $this->input->post('transferbank'),
					'status_payment'		=> 'waiting',
				))->row()->id_invoice,
				'id_customer'			=> $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer,
				'id_seller'				=> $this->db->get_where('cars', array('id_car' => $get_id_car))->row()->id_user,
				'id_car'				=> $get_id_car,
				'date_booking'			=> $get_booking_date,
				'until_date_booking'	=> $get_until_booking_date,
				'price_payment'			=> $this->db->get_where('cars', array('id_car' => $get_id_car))->row()->price,
				'transfer_bank'			=> $this->input->post('transferbank'),
				'is_cancel'				=> null,
				'status'				=> 'waiting payment',
				'is_new_booking'		=> 1,
				'date_booking_update'	=> null,
			);

			$this->db->insert('booking', $booking);

			echo "sukses booking!";
		}
	}
}
