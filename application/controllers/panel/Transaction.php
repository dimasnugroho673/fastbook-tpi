<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaction');

        $this->db->set('is_new_invoice', 0);
        $this->db->where('for_seller', $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer);
        $this->db->update('invoice');
    }

    public function index()
    {
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();

        if ($this->input->get('payment') && $this->input->get('status') != null) {
            echo $this->input->get('payment') . $this->input->get('status');
            die;
        }

        $data['transactions'] = $this->M_transaction->seller_listing($data['check_user']->id_customer)->result();
        $data['view_page'] = 'transaction/index';

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/layout/sidebar', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }

    public function confirm_payment($id_invoice)
    {
        $get_id_invoice = urldecode(decrypt_url($id_invoice));

        if (time() > $this->db->get_where('invoice', array('id_invoice' => $get_id_invoice))->row()->limit_date_payment) {
            $this->session->set_flashdata('status', '<div class="alert alert-danger" role="alert">
                Konfirmasi gagal! invoice dengan nomor <strong>' . $this->db->get_where('invoice', array('id_invoice' => $get_id_invoice))->row()->id_invoice . '</strong> telah expired
            </div>');
            redirect('panel/transaction');
        } else {
            $this->db->set('status_payment', 'success');
            $this->db->set('date_payment', time());
            $this->db->where('id_invoice', $get_id_invoice);
            $this->db->update('invoice');

            $this->db->set('status', 'success payment');
            $this->db->where('id_invoice', $get_id_invoice);
            $this->db->update('booking');

            $this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">
                Invoice dengan nomor <strong>' . $this->db->get_where('invoice', array('id_invoice' => $get_id_invoice))->row()->id_invoice . '</strong> telah berhasil di konfirmasi pembayaran
            </div>');
            redirect('panel/transaction');
        }
    }
}
