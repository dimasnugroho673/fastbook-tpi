<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Home</title>
</head>

<body>
    <div class="row justify-content-center w-100" style="margin-top: 100px;">
        <div class="container">
            <div class="col-md-12">
                <div class="card" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, .1); margin-top: -60px;">
                    <div class="card-header" style="border-bottom: none; background: none;">
                        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;"><i class="fas fa-money-bill-alt mr-2" style="color: #D33036"></i> Cari Mobil Sewaan Anda di
                            Sini
                        </h5>
                    </div>
                    <div class="card-body">
                        <form action="<?= site_url('car/search') ?>" method="get">
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vendor font-14">Merk mobil</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="inputGroupSelect01"><i class="fas fa-car"></i></label>
                                            </div>
                                            <select name="vendor" class="custom-select" id="inputGroupSelect01">
                                                <option selected disabled>Choose...</option>
                                                <?php foreach ($vendor as $v) : ?>
                                                    <option value="<?= $v->vendor ?>"><?= $v->vendor ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vendor font-14">Lokasi anda</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="inputGroupSelect01"><i class="fas fa-map-marker-alt"></i></label>
                                            </div>
                                            <select name="city" class="custom-select" id="inputGroupSelect01">
                                                <option selected disabled>Choose...</option>
                                                <option value="tanjungpinang">Tanjungpinang</option>
                                                <option value="bintan">Bintan</option>
                                                <option value="batam">Batam</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vendor font-14">Jumlah penumpang</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-wheelchair"></i></span>
                                            </div>
                                            <input type="text" name="total_passenger" class="form-control" placeholder="Jumlah penumpang" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-3">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-primary color-primary text-white font-14" style="min-width: 160px; border-radius: 50px;"><i class="fas fa-search mr-2"></i><strong>Cari
                                            mobil</strong> </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <ul>
        <?php foreach ($cars as $c) : ?>
            <li><a href="<?= site_url('home/show/' . $c->id_car) ?>"><?= $c->name_car ?> &mdash; <?= $c->name_customer ?> <br>
                    <?= $c->price ?>
                </a></li>
        <?php endforeach; ?>
    </ul>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>