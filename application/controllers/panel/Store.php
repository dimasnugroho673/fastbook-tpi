<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Store extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
        $data['view_page'] = 'store/index';

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/layout/sidebar', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }
}
