<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Customers</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Customers</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <!-- <div class="row">

            <div class="col-sm-12">
                <div class="card">

                    <div class="card-header">
                        <h5>Hello card</h5>
                        <div class="card-header-right">
                            <div class="btn-group card-option">
                                <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-more-horizontal"></i>
                                </button>
                                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                    <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                    <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                    <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est laborum."
                        </p>
                    </div>
                </div>
            </div>

        </div> -->



        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-unblock-customer" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah dicabut status blokir
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-block-customer" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} seconds ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah diblokir
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-stripped table-hover table-sm" id="dataCustomers">
                    <thead>
                        <tr>
                            <th data-orderable="false">No</th>
                            <th data-orderable="false">Aksi</th>
                            <th>Nama</th>
                            <th>Tipe</th>
                            <th data-orderable="false">Toko mobil</th>
                            <th>Membership</th>
                            <th>Join at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;

                        foreach ($customers as $c) :  ?>
                            <tr>
                                <th><?= $no++; ?></th>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php if ($c->is_active == 99) { ?>
                                                <a class="dropdown-item" href="<?= site_url('admin/customers/unblock/' . urlencode(encrypt_url($c->id_customer))) ?>"><i class="feather icon-delete mr-1"></i>Cabut Blokir</a>
                                            <?php } else if ($c->is_active == 1) { ?>
                                                <a class="dropdown-item btn-block-customer" href="javascript:;" data-id="<?= urlencode(encrypt_url($c->id_customer)) ?>" data-toggle="modal" data-target="#modalBlockCustomer"><i class="feather icon-x-circle mr-1"></i>Blokir</a>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </td>
                                <td><strong><?= $c->name_customer ?> <?php echo ($c->is_active == 99) ? '<span class="badge badge-light-danger ml-1">Diblokir admin</span>' : null; ?> </strong><br>
                                    <small>@<?= $c->username ?> &mdash; <?= $c->email ?></small>
                                </td>
                                <td><?= $c->customer_type ?></td>
                                <td><?= $c->merchant_name ?></td>
                                <td><?php if ($c->account_type == 'trial') { ?>
                                        <span class="badge badge-light-secondary">Trial<i class="feather icon-clock ml-1"></i></span><br>
                                        <small>Trial end at <?= date('d F Y', $c->trial_end) ?></small>
                                    <?php } else { ?>
                                        <span class="badge badge-light-warning">Premium<i class="feather icon-gitlab ml-1"></i></span><br>
                                        <small>Premium end at <?= date('d F Y', $c->token_active_end) ?></small>
                                    <?php } ?>
                                </td>
                                <td><?= date('d F Y', $c->first_join) ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalBlockCustomer" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalBlockCustomerLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/customers/block') ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalBlockCustomerLabel">Kirimkan pesan</h5>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="id_customer" id="id_customer_send" class="form-control">

                    <div class="form-group">
                        <label for="message_send">Pesan</label>
                        <textarea name="message" id="message_send" cols="30" rows="10" class="form-control" placeholder="Tambahkan pesan"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#dataCustomers').dataTable();

        $('#id_customer_send').val(null);
        $('#message_send').val(null);

        $('.btn-block-customer').on('click', function() {


            const id = $(this).data('id');

            $('#id_customer_send').val(id);
            $('#message_send').val(null);
        })

    });
</script>