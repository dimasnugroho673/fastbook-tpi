<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
<style type="text/css">
    .dropzone,
    .dropzone * {
        box-sizing: border-box
    }

    .dropzone {
        position: relative;
        min-height: 300px;
        border-radius: 8px;
        border: 2px dashed rgba(0, 0, 0, .4);
        background-color: rgba(0, 0, 0, .1);
    }

    .dropzone .dz-preview {
        position: relative;
        display: inline-block;
        width: 120px;
        margin: 0.5em
    }

    .dropzone .dz-preview .dz-progress {
        display: block;
        height: 15px;
        border: 1px solid #aaa
    }

    .dropzone .dz-preview .dz-progress .dz-upload {
        display: block;
        height: 100%;
        width: 0;
        background: green
    }

    .dropzone .dz-preview .dz-error-message {
        color: red;
        display: none
    }

    .dropzone .dz-preview.dz-error .dz-error-message,
    .dropzone .dz-preview.dz-error .dz-error-mark {
        display: block
    }

    .dropzone .dz-preview.dz-success .dz-success-mark {
        display: block
    }

    .dropzone .dz-preview .dz-error-mark,
    .dropzone .dz-preview .dz-success-mark {
        position: absolute;
        display: none;
        left: 30px;
        top: 30px;
        width: 54px;
        height: 58px;
        left: 50%;
        margin-left: -27px
    }
</style>


<div class="card">
    <div class="card-header">
        <h4>Upload Gambar <?php $id_car_url = urldecode(decrypt_url($this->input->get('id'))) ?></h4>
    </div>
    <div class="card-body">


        <p>Seret atau pilih banyak gambar sekaligus</p>

        <div class="dropzone">

            <div class="dz-message" style="text-align: center;">
                <a class="btn btn-primary btn-sm mt-4" href="javascript:;" role="button">Pilih file <i class="ml-2 fa fa-upload" aria-hidden="true"></i>
                </a>

            </div>

        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.js"></script>
        <script type="text/javascript">
            Dropzone.autoDiscover = false;

            var foto_upload = new Dropzone(".dropzone", {
                url: "<?php echo site_url('panel/car/uploadPicture/' . $id_car_url) ?>",
                maxFilesize: 2,
                method: "post",
                acceptedFiles: "image/*",
                paramName: "userfile",
                dictInvalidFileType: "Type file not allowed",
                addRemoveLinks: true,
            });


            //Event ketika Memulai mengupload
            foto_upload.on("sending", function(a, b, c) {
                a.token = Math.random();
                c.append("token_picture", a.token);
            });


            //Event ketika foto dihapus
            foto_upload.on("removedfile", function(a) {
                let token = a.token;
                let mentah_id = "<?php echo $id_car_url ?>";
                let id_car = parseInt(mentah_id);
                $.ajax({
                    type: "post",
                    data: {
                        token: token,
                        id_car: id_car
                    },
                    url: "<?php echo site_url('panel/car/removePicture') ?>",
                    cache: false,
                    dataType: 'json',
                    success: function() {
                        console.log("remove picture success");
                    },
                    error: function() {
                        console.log("Error");


                    }
                });
            });
        </script>
    </div>
</div>