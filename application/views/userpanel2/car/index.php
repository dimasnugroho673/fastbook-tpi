<div class="card">
    <div class="card-body">

        <a href="<?= site_url('panel/car/add') ?>" class="genric-btn info-border medium">+ Tambahkan mobil</a>

        <div class="my-3">
            <?= $this->session->flashdata('message') ?>
        </div>


        <div class="col-md-3 float-right">
            <div class="input-group mb-3 input-group-sm">
                <input type="text" class="form-control" placeholder="Cari">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="button-addon2">Button</button>
                </div>
            </div>
        </div>


        <div class="table-reponsive">
            <table id="dataCars" class="table table-hover table-sm display table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Aksi</th>
                        <th scope="col">Mobil - Plat</th>
                        <th scope="col">Transmisi</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($cars)) { ?>
                        <?php
                        $no = 1;
                        foreach ($cars as $data) : ?>
                            <tr>
                                <th scope="row"><?= $no++ ?></th>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-outline-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <small>Pilih</small>
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="<?= site_url('panel/car/detail/' . urlencode(encrypt_url($data->id_car))) ?>"><small>Lihat detail</small></a>
                                            <a class="dropdown-item" href="#"><small>Edit</small></a>
                                            <a class="dropdown-item" href="#"><small>Sembunyikan</small></a>
                                            <a class="dropdown-item" href="#"><small>Hapus</small></a>
                                        </div>
                                    </div>
                                </td>
                                <td><?= $data->name_car ?><br>
                                    <small><strong><?= $data->plat_number ?></strong></small>
                                </td>
                                <td><?= $data->transmission ?></td>
                                <td>Rp. <?= number_format($data->price, 0, ',', '.') ?></td>
                                <td><?= $data->is_ready ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>

            <?php if (empty($cars)) { ?>
                <div class="alert alert-danger text-center" role="alert">
                    Belum ada mobil dimasukkan
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- 
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataCars').DataTable();
    });
</script> -->