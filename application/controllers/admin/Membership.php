<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class Membership extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_customer');
        $this->load->model('M_token');
    }

    public function index()
    {
        $data['view_page'] = 'membership/index';
        $data['title'] = 'Admin &mdash; Membership';

        $data['seller'] = $this->M_customer->adminSellerListing()->result();

        $this->load->view('admin/layout/main', $data);
    }

    private function _generatngToken()
    {
        $n = 7;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';



        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function generateToken()
    {
        $long_month = htmlspecialchars($this->input->post('long_month', TRUE));
        $many_token = htmlspecialchars($this->input->post('many_token', TRUE));

        $get_token_data_from_db = $this->db->get('token_customer')->result();

        for ($i = 0; $i < $many_token; $i++) {
            $this->db->insert('token_customer', array(
                'stock_token'   => $this->_generatngToken(),
                'long_month'    => $long_month,
                'status'        => 'preactive'
            ));
        }

        $this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-generate-token').toast('show');
		});
        </script>");
        $this->session->set_flashdata('many_token', $many_token);
        redirect('admin/membership');
        // var_dump($randomString);
    }

    public function sendToken()
    {
        // $get_id_customer = urldecode(decrypt_url($id_customer));


        if ($this->input->post('long_month')) {
            $get_long_month = htmlspecialchars($this->input->post('long_month', TRUE));
            $get_id_customer = urldecode(decrypt_url($this->input->post('id_customer', TRUE)));

            $get_data_customer = $this->db->get_where('customer', array(
                'id_customer' => $get_id_customer
            ))->row();

            $get_token_from_db = $this->M_token->getSendToken($get_long_month)->row();

            $subject = 'Token Membership';
            $message = "Ini adalah token anda " . $get_token_from_db->stock_token . " Jangan pernah beritahu siapapun token anda termasuk kami. Kami tidak pernah meminta token yang diberikan";

            $this->_sendMail($get_data_customer->email, $get_data_customer->name_customer, $subject, $message);

            $this->db->set('status', 'waiting');
            $this->db->where('stock_token', $get_token_from_db->stock_token);
            $this->db->update('token_customer');

            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-sendtoken-customer').toast('show');
            });
            </script>");
            $this->session->set_flashdata('name_customer', $get_data_customer->name_customer);
            redirect('admin/membership');
        }
    }

    private function _sendMail($email, $name, $subject, $message)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 2;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.computer-cyber.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'admin@computer-cyber.org';                     // SMTP username
            $mail->Password   = 'computercyber2018';                               // SMTP password
            $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('admin@computer-cyber.org', 'Fastbook');
            $mail->addAddress($email, $name);
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
