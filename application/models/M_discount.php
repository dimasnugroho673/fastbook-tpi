<?php

class M_discount extends CI_Model
{
    private $table = "discount_code";

    public function adminDiscountListing()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->get();
    }
}
