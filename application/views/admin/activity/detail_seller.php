<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Activity</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/activity') ?>">Activity</a></li>
                            <li class="breadcrumb-item active">Detail activity</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Aktifitas Booking</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-striped table-hover" id="dataSellerBooking">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Invoice</th>
                                    <th>Untuk seller</th>
                                    <th>Mobil</th>
                                    <th>Tanggal booking</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($bookings as $b) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><a href="" class="text-primary"><?= $b->id_invoice ?></a></td>
                                        <td><?= $b->name_customer ?></td>
                                        <td><?= $b->name_car ?></td>
                                        <td><?= date('d F Y', $b->date_booking) ?> - <?= date('d F Y', $b->until_date_booking) ?></td>
                                        <td><?= $b->status ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>ID Invoice</th>
                                    <th>Untuk seller</th>
                                    <th>Mobil</th>
                                    <th>Tanggal bayar</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Riwayat Pembayaran</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-hover table-striped" id="dataSellerPayment">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Invoice</th>
                                    <th>Dari</th>
                                    <th>Batas tanggal bayar</th>
                                    <th>Tanggal bayar</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($payments as $p) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $p->id_invoice ?></td>
                                        <td><?= $p->name_customer ?></td>
                                        <td><?= date('d F Y H:i:s', $p->limit_date_payment) ?> WIB</td>
                                        <td><?= date('d F Y H:i:s', $p->date_payment) ?> WIB</td>
                                        <td><?= $p->status_payment ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>ID Invoice</th>
                                    <th>Dari</th>
                                    <th>Batas tanggal bayar</th>
                                    <th>Tanggal bayar</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>




<script>
    $(document).ready(function() {
        $('#dataSellerBooking').dataTable({
            "columnDefs": [{
                "orderable": false,
                "targets": 1
            }],
        });

        $('#dataSellerPayment').dataTable({
            "columnDefs": [{
                "orderable": false,
                "targets": 1
            }],
        });

    });
</script>