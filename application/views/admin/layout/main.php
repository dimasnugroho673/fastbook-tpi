<!-- all php declaration -->
<?php $data['get_data_user_login'] = $this->db->get_where('admin', array(
    'email' => $this->session->userdata('email')
))->row();

$data['user_role'] = $this->db->get_where('user_role', array(
    'id_user_role' => $data['get_data_user_login']->role_id
))->row();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title ?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/admin/dist/'); ?>assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url('assets/admin/dist/'); ?>assets/css/style.css">

    <!-- datatables -->
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>datatables/dataTables.bootstrap4.min.css">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <!-- font -->
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <style>
        * {
            font-family: 'Ubuntu', sans-serif;
        }

        .page-header {
            box-shadow: none;
        }

        .pcoded-main-container,
        .pcoded-content {
            background-color: white;
        }

        .card {
            box-shadow: none;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }

        .card:hover {
            box-shadow: none;
        }

        .header-title {
            font-size: 28px;
        }
    </style>

</head>

<body class="">


    <header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed header-dark" style="box-shadow: none;">


        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
            <a href="#!" class="b-brand">

                <img src="<?= base_url('assets/admin/dist/'); ?>assets/images/logo.png" alt="" class="logo">
                <img src="<?= base_url('assets/admin/dist/'); ?>assets/images/logo-icon.png" alt="" class="logo-thumb">
            </a>
            <a href="#!" class="mob-toggler">
                <i class="feather icon-more-vertical"></i>
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
                    <div class="search-bar">
                        <input type="text" class="form-control border-0 shadow-none" placeholder="Search here">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#!" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a>
                </li>
                <li class="nav-item">
                    <strong>Accessed from IP :</strong>
                    <?PHP

                    function getUserIP()
                    {
                        // Get real visitor IP behind CloudFlare network
                        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                        }
                        $client  = @$_SERVER['HTTP_CLIENT_IP'];
                        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                        $remote  = $_SERVER['REMOTE_ADDR'];

                        if (filter_var($client, FILTER_VALIDATE_IP)) {
                            $ip = $client;
                        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                            $ip = $forward;
                        } else {
                            $ip = $remote;
                        }

                        return $ip;
                    }


                    $user_ip = getUserIP();

                    echo $user_ip; // Output IP address [Ex: 177.87.193.134]

                    ?>



                    <?php
                    $user_ip2 = getenv('REMOTE_ADDR');
                    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip2"));
                    $country = $geo["geoplugin_countryName"];
                    $city = $geo["geoplugin_city"];

                    echo $city;
                    ?>

                    <?php
                    $userdevice = "";

                    $mobileAgent = array(
                        "iPhone", "iPod", "Android", "Blackberry",
                        "Opera Mini", "Windows ce", "Nokia", "sony", "Linux", "Windows", "MacOS", "iOS"
                    );
                    for ($i = 0; $i < sizeof($mobileAgent); $i++) {
                        if (stripos($_SERVER['HTTP_USER_AGENT'], $mobileAgent[$i])) {
                            $userdevice = $mobileAgent[$i];
                            break;
                        }
                    }

                    echo $userdevice;
                    ?>

                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li>
                    <div class="dropdown">
                        <a class="dropdown-toggle" href="javascript:;" data-toggle="dropdown"><i class="icon feather icon-bell"></i><span class="badge bg-danger"></span></a>
                        <div class="dropdown-menu dropdown-menu-right notification">
                            <div class="noti-head">
                                <h6 class="d-inline-block m-b-0">Notifications</h6>
                                <div class="float-right">
                                    <a href="javascript:;" class="m-r-10">mark as read</a>
                                    <a href="javascript:;">clear all</a>
                                </div>
                            </div>
                            <ul class="noti-body">
                                <li class="n-title">
                                    <p class="m-b-0 text-center">Tidak ada notifikasi.</p>
                                </li>
                            </ul>
                            <div class="noti-footer">
                                <a href="javascript:;">show all</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="dropdown drp-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= base_url('assets/admin/dist/'); ?>assets/images/avatar/default.png" class="img-radius wid-40" alt="User-Profile-Image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">


                                <div class="row">
                                    <div class="col-3">
                                        <img src="<?= base_url('assets/admin/dist/'); ?>assets/images/avatar/default.png" class="img-radius" alt="User-Profile-Image">
                                    </div>
                                    <div class="col-9">
                                        <span>Hi, <span class="text-capitalize"><?= $data['get_data_user_login']->username ?> </span></span>
                                        <br><span style="font-size:12px;" class="text-muted text-white-50 text-uppercase"><?= $data['user_role']->role_name ?></span>
                                    </div>
                                </div>
                            </div>
                            <ul class="pro-body">
                                <li><a href="" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                <li><a href="<?= site_url('auth/master/forgot') ?>" class="dropdown-item"><i class="feather icon-lock"></i> Reset Password</a></li>
                                <li><strong><a href="<?= site_url('auth/master/logout') ?>" class="dropdown-item text-danger"><i class="feather icon-log-out"></i> Log Out</a></strong></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </header>

    <nav class="pcoded-navbar menupos-fixed menu-dark" style="box-shadow: none;">
        <div class="navbar-wrapper  ">
            <div class="navbar-content scroll-div ">
                <ul class="nav pcoded-inner-navbar ">
                    <li class="nav-item pcoded-menu-caption">
                        <label class="text-uppercase">Navigation</label>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/dashboard') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/customers') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Cutomers</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/membership') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-gitlab"></i></span><span class="pcoded-mtext">Membership</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/activity') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-more-vertical"></i></span><span class="pcoded-mtext">Activity</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/discount') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-tag"></i></span><span class="pcoded-mtext">Discount</span></a>
                    </li>
                    <li class="nav-item pcoded-hasmenu">
                        <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-user-plus"></i></span><span class="pcoded-mtext">User</span></a>
                        <ul class="pcoded-submenu">
                            <li><a href="<?= site_url('admin/users') ?>">List User</a></li>
                            <li><a href="<?= site_url('admin/users/menu') ?>">Menu</a></li>
                        </ul>
                    </li>
                    <!-- <li class="nav-item pcoded-hasmenu">
                        <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Page layouts</span></a>
                        <ul class="pcoded-submenu">
                            <li><a href="layout-vertical.html" target="_blank">Vertical</a></li>
                            <li><a href="layout-horizontal.html" target="_blank">Horizontal</a></li>
                        </ul>
                    </li> -->


                </ul>

                <div class="card text-center">
                    <div class="card-block">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="feather icon-sunset f-40"></i>
                        <h6 class="mt-3">Help?</h6>
                        <p>Please contact us on our email for need any support</p>
                        <a href="#!" target="_blank" class="btn btn-primary btn-sm text-white m-0">Support</a>
                    </div>
                </div>

            </div>
        </div>
    </nav>


    <?php $this->load->view('admin/' . $view_page, $data); ?>


    <!-- Required Js -->
    <script src="<?= base_url('assets/admin/dist/'); ?>assets/js/vendor-all.min.js"></script>
    <script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/bootstrap.min.js"></script>
    <script src="<?= base_url('assets/admin/dist/'); ?>assets/js/pcoded.min.js"></script>

    <!-- Apex Chart -->
    <script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/apexcharts.min.js"></script>

    <!-- custom-chart js -->
    <script src="<?= base_url('assets/admin/dist/'); ?>assets/js/pages/dashboard-main.js"></script>

    <!-- datatables -->
    <script src="<?= base_url('assets/'); ?>datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/'); ?>datatables/dataTables.bootstrap4.min.js"></script>

    <!-- SweetALert -->
    <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert2.all.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/sweetalert/myscript.js"></script>
</body>

</html>