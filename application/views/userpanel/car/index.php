<div class="card">
    <div class="card-header" style="border-bottom: none; background: none;">
        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;">Daftar Mobil Saya</h5>
    </div>
    <div class="card-body">

        <a href="<?= site_url('panel/car/add') ?>" class="btn btn-danger mb-4 font-weight-normal">Tambah</a>

        <div class="my-3">
            <?= $this->session->flashdata('message') ?>
        </div>


        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2 border border-primary">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Total mobil</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php $total_car = $this->db->get_where('cars', ['id_user' => $check_user->id_customer]);

                                                                                    echo $total_car->num_rows(); ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-car-side fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2 border border-success">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Mobil ready</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php $total_car_ready = $this->db->get_where('cars', ['id_user' => $check_user->id_customer, 'is_ready' => 'ready']);

                                                                                    echo $total_car_ready->num_rows(); ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-check-double fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2 border border-info">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                    Mobil dipakai</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php $total_car_active = $this->db->get_where('cars', ['id_user' => $check_user->id_customer, 'is_ready' => 'used']);

                                                                                    echo $total_car_active->num_rows(); ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-key fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2 border border-warning">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Mobil service</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php $total_car_service = $this->db->get_where('cars', ['id_user' => $check_user->id_customer, 'is_ready' => 'service']);

                                                                                    echo $total_car_service->num_rows(); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-car-crash fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="dataCars" class="table table-hover table-sm display table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col" width="20px">No</th>
                        <th scope="col" width="80px">Aksi</th>
                        <th scope="col">Mobil - Plat</th>
                        <th scope="col">Transmisi</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($cars)) { ?>
                        <?php
                        $no = 1;
                        foreach ($cars as $c) : ?>
                            <tr>
                                <th scope="row"><?= $no++ ?></th>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-outline-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <small>Pilih</small>
                                        </a>

                                        <div class="dropdown-menu animate slideIn shadow" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="<?= site_url('panel/car/detail/' . urlencode(encrypt_url($c->id_car))) ?>"><small>Lihat detail</small></a>
                                            <a class="dropdown-item" href="<?= site_url('panel/car/ap?id=' . urlencode(encrypt_url($c->id_car))) ?>"><small>Tambah gambar lain</small></a>
                                            <a class="dropdown-item" href="<?= site_url('panel/car/edit/' . urlencode(encrypt_url($c->id_car))) ?>"><small>Edit</small></a>
                                            <a class="dropdown-item" href="#"><small>Sembunyikan</small></a>
                                            <a class="dropdown-item" href="#"><small>Hapus</small></a>
                                        </div>
                                    </div>
                                </td>
                                <td><?= $c->name_car ?><br>
                                    <small><strong><?= $c->plat_number ?></strong></small>
                                </td>
                                <td><?= $c->transmission ?></td>
                                <td>Rp. <?= number_format($c->price, 0, ',', '.') ?></td>
                                <td><?php if ($c->is_ready == 'ready') : ?>
                                        <span class="badge badge-success">Ready</span>
                                    <?php else : ?>

                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>

            <?php if (empty($cars)) { ?>
                <div class="alert alert-danger text-center" role="alert">
                    Belum ada mobil dimasukkan
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#dataCars').DataTable();
    });
</script>