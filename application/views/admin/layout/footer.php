<!-- Required Js -->
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/vendor-all.min.js"></script>
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/bootstrap.min.js"></script>
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/pcoded.min.js"></script>

<!-- Apex Chart -->
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/apexcharts.min.js"></script>


<!-- custom-chart js -->
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/pages/dashboard-main.js"></script>

<!-- datatables -->
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin/dist/'); ?>assets/js/plugins/dataTables.bootstrap4.min.js"></script>


</body>

</html>