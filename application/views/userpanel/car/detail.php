<div class="container">
    <div class="row my-5">
        <div class="col-md-12">

            <a href="<?= site_url('panel/car') ?>" class="btn btn-primary btn-sm my-3"><i class="mr-2 fa fa-arrow-left"></i>
                Kembali</a>

            <div class="card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gambar Mobil</label>
                                <?php if ($detail_car->picture == null) { ?>
                                    <div class="jumbotron jumbotron-fluid col-md-10 rounded" style="border: 3px rgba(0, 0, 0, .3) dashed">
                                        <div class="container">
                                            <p class="text-center align-middle">Belum ada gambar ditambahkan.</p>
                                        </div>
                                    </div>
                                <?php } else { ?>

                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>No Plat</label>
                                <div class="card col-md-8 rounded" style="background-color: black">
                                    <div class="card-body my-3 rounded" style="border: 3px white solid">
                                        <h2 class="text-center text-white align-middle"><?= $detail_car->plat_number ?></h2>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Fitur Tambahan</label>
                                <div class="ml-3">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value="1" name="ac" <?= ($detail_car->ac == 1) ? "checked" : null; ?>> AC </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value="1" name="abs" <?= ($detail_car->abs == 1) ? "checked" : null; ?>> ABS </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value="1" name="day_running_light" <?= ($detail_car->day_running_light == 1) ? "checked" : null; ?>> Day Running Light </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>