<div class="card">
    <div class="card-header" style="border-bottom: none; background: none;">
        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;">Token confirm</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <table class="table table-stripped table-hover">
                    <thead>
                        <tr>
                            <th>Token</th>
                            <th>Durasi</th>
                            <th>Akun Premium aktif sampai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $token; ?></td>
                            <td><?= $get_data_token->long_month ?> Bulan</td>
                            <td><?= date('d F Y', time() + 60 * 60 * 24 * 30 * $get_data_token->long_month)  ?></td>
                        </tr>
                    </tbody>
                </table>

                <form action="<?= site_url('panel/membership/confirmtoken') ?>" method="post">
                    <input type="hidden" name="token_confirm" class="form-control" placeholder="No. Token" value="<?= $token ?>" required>
                    <input type="hidden" name="token_active_end" class="form-control" placeholder="No. Token" value="<?= time() + 60 * 60 * 24 * 30 * $get_data_token->long_month  ?>" required>
                    <div class="text-center">
                        <button type="submit" class="shadow btn btn-danger mt-2">Konfirmasi</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>