<div class="auth-wrapper">

    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center text-center">
                <div class="col-md-12">
                    <div class="card-body">
                        <!-- <img src="assets/images/logo-dark.png" alt="" class="img-fluid mb-4"> -->
                        <h4 class="mb-3 f-w-400">Reset your password</h4>

                        <?php if ($this->session->flashdata('message')) { ?>
                            <?= $this->session->flashdata('message') ?>
                        <?php } ?>

                        <form action="<?= site_url('auth/master/forgot') ?>" method="post">
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control rounded-right <?php echo (form_error('email')) ? "is-invalid" : null; ?>" placeholder="Email address" required>
                                <div class="invalid-feedback">
                                    <?= form_error('email') ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary mb-1">Reset password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>