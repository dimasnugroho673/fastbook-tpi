<div class="card">
    <div class="card-header" style="border-bottom: none; background: none;">
        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;">Membership</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <table class="table table-sm table-hover table-stripped">
                    <thead>
                        <tr>
                            <th>Token</th>
                            <th>Masa aktif</th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($my_token)) { ?>
                            <?php foreach ($my_token as $mt) : ?>
                                <tr>
                                    <td><?= $mt->stock_token ?></td>
                                    <td><?= $mt->long_month ?> Bulan</td>
                                    <td><?= $mt->status ?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php } ?>
                    </tbody>
                </table>

                <?php if (empty($my_token)) { ?>
                    <div class="alert alert-danger text-center" role="alert">
                        Belum ada token berlangganan
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <form action="<?= site_url('panel/membership/confirmtoken') ?>" method="post">
                            <label for="input_token">Masukkan token</label>
                            <input type="text" name="token_code" class="form-control" placeholder="No. Token" required>
                            <small id="tokenHelp" class="form-text text-muted">Masukkan token yang dikirim ke inbox anda.</small>
                            <div class="text-center">
                                <button type="submit" class="shadow btn btn-danger mt-2">Aktifkan Token</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>