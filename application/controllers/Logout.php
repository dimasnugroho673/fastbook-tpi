<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{
    public function index()
    {
        $this->session->unset_userdata('email');

        $this->session->set_flashdata('message', 'Anda berhasil keluar');
        redirect('login/seller');
    }
}
