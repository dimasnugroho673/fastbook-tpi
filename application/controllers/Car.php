<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Car extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_car');
    }

    public function index()
    {
        $data['cars'] = $this->M_car->get_cars()->result();
        $data['vendor'] = $this->M_car->getAllVendor()->result();

        $this->load->view('user/car/index', $data);
    }

    public function search()
    {
        $data['cars'] = $this->M_car->resultCar($this->input->get('city'), $this->input->get('vendor'), $this->input->get('total_passenger'))->result();
        $data['vendor'] = $this->M_car->getAllVendor()->result();

        $this->load->view('user/car/index', $data);
    }

    public function show($id_car)
    {
        $get_id_car = urldecode(decrypt_url($id_car));
        $data['car'] = $this->M_car->get_car($get_id_car)->row();
        $data['check_login'] = function () {
            if ($this->session->userdata('email') != null) {
                return true;
            } else {
                return false;
            }
        };

        $data['is_login'] = $data['check_login']();

        $this->load->view('user/car/detail_car', $data);
    }
}
