<div class="card">
    <div class="card-header" style="border-bottom: none; background: none;">
        <h5 class="font-weight-bold pt-2 pb-2 pl-2" style="font-weight: 600;">Tambah mobil baru</h5>
    </div>
    <div class="card-body">



        <form class="mt-4" action="<?= site_url('panel/car/add') ?>" method="post" enctype="multipart/form-data">
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="plat_number">No Plat</label>
                        <input type="text" name="plat_number" id="plat_number" class="form-control" placeholder="BP 2382 MX">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label for="name_car">Nama Mobil</label>
                        <input type="text" name="name_car" id="name_car" class="form-control" placeholder="Cth. Grand Xenia">
                    </div>
                </div>

            </div>
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="vendor">Vendor</label>
                        <input type="text" name="vendor" id="vendor" class="form-control" placeholder="Cth. Toyota/Suzuki">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="year_of_assembly">Tahun Keluaran</label>
                        <input type="number" name="year_of_assembly" id="year_of_assembly" class="form-control" placeholder="Cth. 2019">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="machine_type">Tipe Mesin</label>
                        <select class="custom-select" name="machine_type" id="machine_type">
                            <option disabled selected>Mesin...</option>
                            <option value="Diesel">Diesel</option>
                            <option value="Bensin">Bensin</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="color">Warna</label>
                        <input type="text" name="color" id="color" class="form-control" placeholder="Red/Jet black">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="transmission">Transmisi</label>
                        <select class="custom-select" name="transmission" id="transmisison">
                            <option disabled selected>Transmisi...</option>
                            <option value="Automatic">Automatic</option>
                            <option value="Manual">Manual</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="total_passenger">Penumpang</label>
                        <input type="number" name="total_passenger" id="total_passenger" class="form-control" placeholder="Cth. 8">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="other_features">Fitur Tambahan</label>
                        <div class="ml-3">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="ac"> AC </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="abs"> ABS </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="day_running_light"> Day Running Light </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Deskripsi Tambahan</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="Masukkan keterangan"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="picture">Gambar</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="picture" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">Pilih atau seret file . . . </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="price">Harga per Hari</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Rp</span>
                            </div>
                            <input type="number" name="price" id="price" class="form-control" placeholder="Cth. 500000">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-4">
                    <div class="form-group ml-3">
                        <label for="price">Status mobil</label>
                        <div class="form-radio">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="target" id="membershipRadios1" value="1">Lulus</label>
                        </div>
                        <div class="form-radio">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="target" id="membershipRadios2" value="0">Tidak lulus</label>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>