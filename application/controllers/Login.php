<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function customer()
	{
		$this->load->view('user/login_customer');
	}

	public function customer_action()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password_customer');

		$check_account_on_db = $this->db->get_where('customer', array(
			'email' 			=> $email,
			'customer_type'		=> 'customer'
		))->row();

		if ($check_account_on_db) {
			$this->session->set_flashdata('message', 'Akun anda benar, anda berhasil masuk');

			if ($check_account_on_db->is_active == 1) {

					$this->_login('customer');

			} elseif ($check_account_on_db->is_active == 0) {
				$this->session->set_flashdata('message', 'Akun anda belum diaktivasi!');
				redirect('login/customer');
			} elseif ($check_account_on_db->is_active == 99) {
				$this->session->set_flashdata('message', 'Akun anda telah diblokir oleh admin!');
				redirect('login/customer');
			}
		} else {
			$this->session->set_flashdata('message', 'Akun tidak ditemukan!');
			redirect('login/customer');
		}
	}

	public function seller()
	{
		$this->load->view('user/login_seller');
	}

	public function seller_action()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password_customer');

		$check_account_on_db = $this->db->get_where('customer', array(
			'email' 			=> $email,
			'customer_type'		=> 'seller'
		))->row();

		if ($check_account_on_db) {
			$this->session->set_flashdata('message', 'Akun anda benar, anda berhasil masuk');

			if ($check_account_on_db->is_active == 1) {

				if ($check_account_on_db->is_trial == 1) {
					$this->session->set_flashdata('message', 'Akun anda masih dalam masa trial');
					$this->_login('seller');
				} elseif ($check_account_on_db->is_trial == 0) {

					if ($check_account_on_db->token_active_end > time()) {
						$this->session->set_flashdata('message', 'Akun anda premium');

						$this->_login('seller');
					} elseif ($check_account_on_db->token_active_end <= time()) {
						$this->session->set_flashdata('message', 'Masa akun anda premium anda telah habis!');
						redirect('login/seller');
					}
				}
			} elseif ($check_account_on_db->is_active == 0) {
				$this->session->set_flashdata('message', 'Akun anda belum diaktivasi!');
				redirect('login/seller');
			} elseif ($check_account_on_db->is_active == 99) {
				$this->session->set_flashdata('message', 'Akun anda telah diblokir oleh admin!');
				redirect('login/seller');
			}
		} else {
			$this->session->set_flashdata('message', 'Akun tidak ditemukan!');
			redirect('login/seller');
		}
	}

	private function _login($customer_type)
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password_customer');

		$check_account_on_db = $this->db->get_where('customer', array(
			'email' 			=> $email,
			'customer_type'		=> $customer_type
		))->row();

		if (password_verify($password, $check_account_on_db->password_customer)) {
			$data = array(
				'email' => $email
			);

			$this->session->set_userdata($data);
			if ($customer_type == 'seller') {
				redirect('panel/my');
			} else {
				redirect('/car');
			}
			
		} else {
			$this->session->set_flashdata('message', 'Password salah');
			redirect('login/' . $customer_type);
		}
	}
}
