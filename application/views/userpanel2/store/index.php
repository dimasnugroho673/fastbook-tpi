<div class="card">
    <div class="card-body">
        <div class="jumbotron text-center">
            <img src="https://buildwithangga.com/themes/front/images/logo-bwa.png" alt="..." class="rounded-circle d-xs-none" width="100px">
            <h6 class="mt-3"><strong><?= $check_user->merchant_name ?></strong><i class="ml-1 fa fa-check-circle"></i></h6>
            <small class="font-weight-light">Bergabung sejak <?= date('d F Y', $check_user->first_join)   ?></small>
        </div>

    </div>
</div>