<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_user');
    }

    public function index()
    {
        $data['view_page'] = 'user/index';
        $data['title'] = 'Admin &mdash; Users';

        $data['users'] = $this->M_user->adminUserListing()->result();

        $data['check_super_admin_ready'] = $this->db->get_where('admin', array(
            'role_id' => 1,
        ));

        if ($data['check_super_admin_ready']->num_rows() > 0) {
            $data['roles'] = $this->db->get_where('user_role', array(
                'id_user_role !=' => 1,
            ))->result();
        } else {
            $data['roles'] = $this->db->get_where('user_role', array(
                'id_user_role !=' => 0,
            ))->result();
        }


        $this->form_validation->set_rules('name_user', 'Nama user', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/layout/main', $data);
        } else {
            $this->_reguser();
        }
    }

    private function _reguser()
    {
        $dataToken = base64_encode(random_bytes(32));
        $userEmail = htmlspecialchars($this->input->post('email_user', TRUE));
        $userRole = htmlspecialchars($this->input->post('role_id', TRUE));

        $check_role_on_db = $this->db->get_where('user_role', array(
            'id_user_role' => $userRole
        ))->row();

        $dataInsert = array(
            'user_reg_token'   => $dataToken,
            'email_user'        => $userEmail,
            'role_id'           => $userRole,
            'date_created'      => time()
        );

        $this->db->insert('user_registration_token', $dataInsert);

        $this->_sendMail($userEmail, htmlspecialchars($this->input->post('name_user', TRUE)), $check_role_on_db->role_name, $dataToken);
        $this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-add-new-user').toast('show');
		});
        </script>");
        $this->session->set_flashdata('name_user', htmlspecialchars($this->input->post('name_user', TRUE)));
        redirect('admin/users');
    }

    private function _greetingPeople()
    {
        date_default_timezone_set("Asia/Jakarta");

        $b = time();
        $hour = date("G", $b);

        if ($hour >= 0 && $hour <= 11) {
            return "Selamat Pagi";
        } elseif ($hour >= 12 && $hour <= 14) {
            return "Selamat Siang";
        } elseif ($hour >= 15 && $hour <= 17) {
            return "Selamat Sore";
        } elseif ($hour >= 17 && $hour <= 18) {
            return "Selamat Petang";
        } elseif ($hour >= 19 && $hour <= 23) {
            return "Selamat Malam";
        }
    }

    private function _sendMail($email, $name, $role, $token)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 2;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.computer-cyber.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'admin@computer-cyber.org';                     // SMTP username
            $mail->Password   = 'computercyber2018';                               // SMTP password
            $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = '465';                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('admin@computer-cyber.org', 'Fastbook');
            $mail->addAddress($email, $name);
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            $mail->addAttachment('./Cara Registrasi Sistem Panel Fastbook.pdf');         // Add attachments

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Registrasi akun $role";
            $mail->Body    = "Hi " . $this->_greetingPeople() . " $name, kami mengundang anda untuk menjadi bagian dari kami, admin telah mendaftarkan anda menjadi $role pada Fastbook. Silahkan <a href='" . base_url() . "registration/user?email=$email&token=" . urlencode($token) . "'><b>Klik tautan ini</b></a> untuk dapat mendaftar menjadi $role. Jika anda kurang mengerti anda dapat mengunduh dokumen yang telah kami berikan mengenai tata cara registrasi. <br><br><br> Terima Kasih.";
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();

            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-add-new-user').toast('show');
            });
            </script>");
            $this->session->set_flashdata('name_user', $name);
            redirect('admin/users');
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
