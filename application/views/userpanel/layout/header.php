<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>bootstrap4.5.0/css/bootstrap.min.css">

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,400&display=swap" rel="stylesheet">

    <!-- icons -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('assets/') ?>userpanel/assets/css/style.css">

    <!-- datatables -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/datatables/dataTables.bootstrap4.min.css">

    <!-- jquery -->
    <script src="<?= base_url('assets') ?>/jquery/jquery-3.5.1.min.js"></script>

    <title>Fastbook</title>

    <style>
        * {
            font-family: 'Open Sans', sans-serif;
        }

        body {
            background-color: #F5F6FA;
        }

        .nav-link {
            font-weight: 700;
        }

        .nav-link,
        .font-14 {
            font-size: 14px !important;
        }

        .navbar-nav .active {
            font-weight: 600;
            color: #D33036 !important;
        }

        .color-primary {
            background-color: #D33036 !important;
        }

        .btn-login {
            background-color: #D33036 !important;
            font-weight: 600 !important;
        }

        .jumbotron {
            height: 400px !important;
        }

        .list-group .active {
            font-weight: 700;
        }

        .breadcrumb .breadcrumb-item {
            font-size: 13px !important;
        }

        .btn-danger {
            color: #fff;
            background-color: #dc3545;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
            border-radius: .3rem;
        }
    </style>
</head>

<body>


    <!-- ================== HEADER =================== -->



    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="#">Logo fastbook</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Utama <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Mobil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Angkutan Umum</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <!-- <li class="nav-item">
						<a class="nav-link" href="#">Membership</a>
					</li> -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-lg-inline text-gray-600">Hi, Dimas</span>
                            <img class="img-profile rounded-circle" style="max-width: 30px;" src="avatar/default.png">
                        </a>

                        <!-- <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="mr-2 d-none d-lg-inline text-gray-600 small">Hi, Dimas</span>
							<img src="avatar/default.png" class="rounded-circle img-profile d-block" alt="..."
								style="max-width: 30px;">
						</a> -->
                        <div class="dropdown-menu animate slideIn shadow">
                            <a class="dropdown-item font-14" href="<?= site_url('panel/my') ?>">Panel saya</a>
                            <a class="dropdown-item font-14" href="<?= site_url('membership') ?>">Membership</a>
                            <a class="dropdown-item font-14" href="<?= site_url('logout') ?>">Logout</a>
                        </div>
                    </li>
                </ul>
                <!-- <a href="" class="btn btn-default btn-sm btn-login text-white"
					style="border-radius: 50px; min-width: 80px;">Login</a> -->
            </div>
        </div>

    </nav>



    <!-- ================== END HEADER =================== -->

    <div class="jumbotron jumbotron-fluid" style="background-image: url('https://ik.imagekit.io/tvlk/image/imageResource/2019/11/29/1574993189791-804afe416b0604b30f51c74193eb57c5.png?tr=q-75'); background-size: cover; margin-top: 58px;">
        <div class="container">
        </div>
    </div>


    <div class="row justify-content-center w-100 mb-5" style="min-width: 100%;">
        <div class="container">
            <div class="col-md-12">
                <div class="card" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, .1); margin-top: -98px;">
                    <div class="card-header" style="border-bottom: none; background: none;">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb" style="background: none;">
                                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                                <li class="breadcrumb-item text-capitalize"><a href="<?= site_url('panel/my') ?>"><?= $this->uri->segment(1) ?></a></li>
                                <?php if ($this->uri->segment(2)) : ?>
                                    <li class="breadcrumb-item text-capitalize"><a href="<?= site_url('panel/my') ?>"><?= $this->uri->segment(2) ?></a></li>
                                <?php endif ?>
                                <?php if ($this->uri->segment(3)) : ?>
                                    <li class="breadcrumb-item text-capitalize active" aria-current="page"><?= $this->uri->segment(3) ?></li>
                                <?php endif ?>
                            </ol>
                        </nav>
                    </div>
                    <div class="card-body">
                        <h4 class="font-weight-bold pb-2 pl-3" style="margin-top: -40px;">Panel Saya</h4>

                    </div>
                </div>
            </div>
        </div>
    </div>