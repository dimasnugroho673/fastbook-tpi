<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Membership</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Membership</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-sendtoken-customer" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Token telah dikirim ke akun <strong><?= $this->session->flashdata('name_customer'); ?></strong>
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-generate-token" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Token baru berhasil di generate sebanyak <strong><?= $this->session->flashdata('many_token'); ?></strong> token
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning" role="alert">
                    <i class="feather icon-info mr-1"></i> Demi keamanan sistem, admin tidak diperkenankan melihat daftar Token. Hal ini berlaku untuk mmenghindari tindak kecurangan. Harap baca <a href="">Aturan pakai sistem.</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>List Token</h5>
                    </div>
                    <div class="card-body">
                        <form action="<?= site_url('admin/membership/generatetoken') ?>" method="POST">
                            <div class="form-group row">
                                <label for="long_month" class="col-sm-3 col-form-label">Masa aktif Token </label>
                                <div class="col-sm-4">
                                    <select name="long_month" class="custom-select my-1 ml-sm-2" id="long_month" required>
                                        <option selected>Pilih...</option>
                                        <option value="6">6 Bulan</option>
                                        <option value="12">12 Bulan</option>
                                        <option value="24">24 Bulan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="many_token" class="col-sm-3 col-form-label">Banyak Token</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="many_token" id="many_token" placeholder="Banyak Token" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <button class="btn btn-sm btn-primary event-btn m-2" type="submit">
                                        <span class="spinner-border spinner-border-sm mr-1" role="status"></span>
                                        <span class="load-text">Generating...</span>
                                        <span class="btn-text"><i class="feather icon-refresh-cw mr-1"></i> Generate Token</span>
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Token</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover m-b-0">
                                <thead>
                                    <tr>
                                        <th><span>Bulan</span></th>
                                        <th><span>preAktif <a class="help" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="feather icon-help-circle f-16"></i></a></span></th>
                                        <th><span>Aktif <a class="help" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="feather icon-help-circle f-16"></i></a></span></th>
                                        <th><span>Expired <a class="help" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"><i class="feather icon-help-circle f-16"></i></a></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>6 Bulan</td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 6,
                                                'status' => 'preactive'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 6,
                                                'status' => 'active'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 6,
                                                'status' => 'expired'
                                            ])->num_rows(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>12 Bulan</td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 12,
                                                'status' => 'preactive'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 12,
                                                'status' => 'active'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 12,
                                                'status' => 'expired'
                                            ])->num_rows(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>24 Bulan</td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 24,
                                                'status' => 'preactive'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 24,
                                                'status' => 'active'
                                            ])->num_rows(); ?></td>
                                        <td><?php echo $this->db->get_where('token_customer', [
                                                'long_month' => 24,
                                                'status' => 'expired'
                                            ])->num_rows(); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Total token di Database</strong> </td>
                                        <td colspan="3" align="center"><strong><?php echo $this->db->get('token_customer')->num_rows(); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php echo $this->session->flashdata('status'); ?>

        <div class="alert alert-info my-3" role="alert">
            <i class="feather icon-info mr-1"></i> Membership hanya berlaku bagi seller.
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-unblock-customer" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah dicabut status blokir
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-block-customer" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} seconds ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Akun <strong><?= $this->session->flashdata('name_customer'); ?></strong> Telah diblokir
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-stripped table-hover table-sm" id="dataCustomers">
                    <thead>
                        <tr>
                            <th data-orderable="false">No</th>
                            <th data-orderable="false">Aksi</th>
                            <th>Nama</th>
                            <th data-orderable="false">Toko mobil</th>
                            <th>Membership</th>
                            <th>Join at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;

                        foreach ($seller as $s) :  ?>
                            <tr>
                                <th><?= $no++; ?></th>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <!-- <a class="dropdown-item" href="<?= site_url('admin/membership/sendtoken/' . urlencode(encrypt_url($s->id_customer))) ?>"><i class="feather icon-upload mr-1"></i>Kirim Token</a> -->
                                            <a href="javascript:;" class="dropdown-item btn-send-token" data-id="<?= urlencode(encrypt_url($s->id_customer)) ?>" data-toggle="modal" data-target="#modalSendToken">
                                                <i class="feather icon-upload mr-1"></i>Kirim Token
                                            </a>

                                        </div>
                                    </div>
                                </td>
                                <td><strong><?= $s->name_customer ?> <?php echo ($s->is_active == 99) ? '<span class="badge badge-light-danger ml-1">Diblokir admin</span>' : null; ?> </strong><br>
                                    <small>@<?= $s->username ?> &mdash; <?= $s->email ?></small>
                                </td>
                                <td><?= $s->merchant_name ?></td>
                                <td><?php if ($s->account_type == 'trial') { ?>
                                        <span class="badge badge-light-secondary">Trial<i class="feather icon-clock ml-1"></i></span><br>
                                        <small>Trial end at <?= date('d F Y', $s->trial_end) ?></small>
                                    <?php } else { ?>
                                        <span class="badge badge-light-warning">Premium<i class="feather icon-gitlab ml-1"></i></span>
                                    <?php } ?>
                                </td>
                                <td><?= date('d F Y', $s->first_join) ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalSendToken" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalSendTokenLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/membership/sendtoken/' . urlencode(encrypt_url($s->id_customer))) ?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalSendTokenLabel">Pilih masa aktif token</h5>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="long_month">Masa aktif Token </label>
                        <input type="hidden" name="id_customer" id="id_customer_send" class="form-control">
                        <select name="long_month" class="custom-select" id="long_month" required>
                            <option selected>Pilih...</option>
                            <option value="6">6 Bulan</option>
                            <option value="12">12 Bulan</option>
                            <option value="24">24 Bulan</option>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-primary event-btn m-2" type="submit">
                        <span class="spinner-border spinner-border-sm mr-1" role="status"></span>
                        <span class="load-text">Mengririm...</span>
                        <span class="btn-text">Kirim Token</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#dataCustomers').dataTable();

        $('#id_customer_send').val(null);

        $('.btn-send-token').on('click', function() {
            const id = $(this).data('id');

            $('#id_customer_send').val(id);
        })

    });
</script>