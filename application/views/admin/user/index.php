<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <p class="m-b-10 lead header-title">Users</p>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('admin/dahsboard') ?>"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item active">List Users</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-new-user" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Link registrasi telah dikirim ke email <strong><?= $this->session->flashdata('name_user'); ?></strong>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUserModal">
                    Tambah user
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-hover table-sm" id="dataUsers">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Role</td>
                            <td>No HP</td>
                            <td>Status</td>
                            <td>Join at</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($users as $u) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $u->username ?><br>
                                    <small class="text-muted"><?= $u->email ?></small>
                                </td>
                                <td><span class="text-primary"><?= $u->role_name ?></span></td>
                                <td><?= $u->no_hp ?></td>
                                <td><?php if ($u->is_active == 1) {  ?>
                                        <span class="badge badge-light-success">Aktif</span>
                                    <?php } elseif ($u->is_active == 0) { ?>
                                        <span class="badge badge-light-secondary">Non aktif</span>
                                    <?php } elseif ($u->is_active == 99) { ?>
                                        <span class="badge badge-light-danger">Blocked</span>
                                    <?php } ?>
                                </td>
                                <td><?= date('d F Y', $u->joined_at) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Role</td>
                            <td>No HP</td>
                            <td>Status</td>
                            <td>Join at</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>




    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="addUserModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('admin/users') ?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="addUserModalLabel">Tambah User</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name_user">Nama User</label>
                        <input type="text" name="name_user" id="name_user" class="form-control" placeholder="Cth. Dadang Suratang">
                    </div>
                    <div class="form-group">
                        <label for="email_user">Email User</label>
                        <input type="email" name="email_user" id="email_user" class="form-control" placeholder="mail@domaiin.com">
                    </div>
                    <div class="form-group">
                        <label for="role_id">Role User</label>
                        <select name="role_id" id="role_id" class="form-control">
                            <option selected disabled>Pilih role</option>
                            <?php foreach ($roles as $r) : ?>
                                <option value="<?= $r->id_user_role ?>"><?= $r->role_name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataUsers').dataTable();

    });
</script>