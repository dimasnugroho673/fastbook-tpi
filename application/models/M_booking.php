<?php

class M_booking extends CI_Model
{
    public function seller_listing($check_user)
    {
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->join('customer', 'booking.id_seller = customer.id_customer');
        $this->db->join('cars', 'booking.id_seller = cars.id_user');
        $this->db->join('invoice', 'invoice.id_invoice = booking.id_invoice');
        $this->db->where('booking.id_seller', $check_user);
        return $this->db->get();
    }

    public function adminCustomerBookingMonitor($id_customer)
    {
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->join('customer', 'booking.id_seller = customer.id_customer');
        $this->db->join('cars', 'booking.id_seller = cars.id_user');
        $this->db->join('invoice', 'invoice.id_invoice = booking.id_invoice');
        $this->db->where('booking.id_customer', $id_customer);
        return $this->db->get();
    }

    public function adminSellerBookingMonitor($id_customer)
    {
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->join('customer', 'booking.id_seller = customer.id_customer');
        $this->db->join('cars', 'booking.id_seller = cars.id_user');
        $this->db->join('invoice', 'invoice.id_invoice = booking.id_invoice');
        $this->db->where('booking.id_seller', $id_customer);
        return $this->db->get();
    }

    public function adminSellerPaymentMonitor($id_customer)
    {
        $this->db->select('*');
        $this->db->from('invoice');
        $this->db->join('customer', 'invoice.for_seller = customer.id_customer');
        $this->db->join('cars', 'invoice.for_seller = cars.id_user');
        $this->db->where('invoice.for_seller', $id_customer);
        return $this->db->get();
    }
}
