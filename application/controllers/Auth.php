<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class Auth extends CI_Controller
{
    public function master($url = null, $url2 = null)
    {
        $data['view_page'] = 'auth/login';
        $data['title'] = 'Login &mdash; Master';


        if ($url == "logout") {
            $this->_logout();
        } else if ($url == "forgot") {
            if ($url2 == null) {
                $this->_forgotPassword();
            } else if ($url2 == "my") {
                $this->_changePassword();
            }
        } else if ($url == null) {

            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == false) {
                $this->load->view('admin/auth/main', $data);
            } else {
                $get_email = $this->input->post('email',  TRUE);
                $get_password = $this->input->post('password',  TRUE);

                $check_user_on_db = $this->db->get_where('admin', array(
                    'email' => $get_email
                ))->row();

                if ($check_user_on_db) {
                    if ($check_user_on_db->is_active == 1) {
                        $this->_login($get_email, $get_password);
                    } elseif ($check_user_on_db->is_active == 0) {
                        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
                        <i class="feather icon-alert-circle mr-1"></i> Akun anda tidak aktif. Hubungi <a href="">admin@computer-cyber.org</a> untuk melakukan pengaduan lebih lanjut.
                       </div>');
                        redirect('auth/master');
                    } elseif ($check_user_on_db->is_active == 99) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        <i class="feather icon-alert-circle mr-1"></i> Akun telah diblokir oleh admin. Hubungi <a href="">admin@computer-cyber.org</a> untuk melakukan pengaduan lebih lanjut.
                       </div>');
                        redirect('auth/master');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <i class="feather icon-alert-circle mr-1"></i> Akun tidak ditemukan
                   </div>');
                    redirect('auth/master');
                }
            }
        }
    }

    private function _login($email, $password)
    {

        $check_user_on_db = $this->db->get_where('admin', array(
            'email'             => $email
        ))->row();

        if (password_verify($password, $check_user_on_db->password)) {
            $data = array(
                'email' => $email
            );

            $this->session->set_userdata($data);

            redirect('admin/dashboard');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            <i class="feather icon-alert-circle mr-1"></i> Password atau email salah
           </div>');
            redirect('auth/master');
        }
    }

    private function _forgotPassword()
    {
        $data['view_page'] = 'auth/forgot';
        $data['title'] = 'Login &mdash; Forgot Password';

        $this->load->view('admin/auth/main', $data);

        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/auth/main', $data);
        } else {
            $email = $this->input->post('email', TRUE);
            $dataToken = base64_encode(random_bytes(32));

            $dataInsert = array(
                'user_forgot_pass_token'   => $dataToken,
                'email_user'        => $email,
                'date_created'      => time()
            );

            $this->db->insert('user_forgot_password_token', $dataInsert);
            $this->_sendMail($email, $dataToken);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            <i class="feather icon-check mr-1"></i> Link reset password telah dikirim via email. Cek email anda dalam 1x24 jam.
            </div>');
            redirect('auth/master/forgot');
        }
    }

    private function _changePassword()
    {
        $email = $this->db->get_where('user_forgot_password_token', array(
            'email_user' => $this->input->get('email')
        ))->row();

        $token = $this->db->get_where('user_forgot_password_token', array(
            'user_forgot_pass_token' => $this->input->get('token')
        ))->row();

        if ($this->input->get('email') == null || $this->input->get('token') == null) {
            echo "HTTP/1.1 404 Not Found";
        } else {
            if (!($email) || !($token)) {
                echo "Authentikasi gagal";
            } else {
                if (time() - $token->date_created < (60 * 60 * 24)) {

                    $user = $this->db->get_where('admin', array(
                        'email' => $this->input->get('email')
                    ))->row();

                    $data['view_page'] = 'auth/change_password';
                    $data['title'] = 'Reset Password &mdash; ' . $user->username;

                    $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]');
                    $this->form_validation->set_rules('password2', 'Password', 'trim|required|matches[password1]');

                    if ($this->form_validation->run() == false) {
                        $this->load->view('admin/auth/main', $data);
                    } else {

                        $dataInsert = array(
                            'password'    => password_hash($this->input->post('password2', TRUE), PASSWORD_DEFAULT)
                        );

                        $this->db->update('admin', $dataInsert);

                        $this->db->delete('user_forgot_password_token', array(
                            'user_forgot_pass_token' => $this->input->get('token')
                        ));
                        $this->session->set_flashdata('status_success_reset_password', "Password telah diubah, silahkan login untuk masuk kembali ke sistem Fastbook :)");
                        redirect('auth/master');
                    }
                } else {
                    $this->db->delete('user_forgot_password_token', array(
                        'email_user' => $this->input->get('email')
                    ));

                    echo "Link expired!";
                }
            }
        }
    }

    private function _sendMail($email, $token)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 2;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.computer-cyber.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'admin@computer-cyber.org';                     // SMTP username
            $mail->Password   = 'computercyber2018';                               // SMTP password
            $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('admin@computer-cyber.org', 'Fastbook');
            $mail->addAddress($email, '');
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Permintaan reset password";
            $mail->Body    = "Hai, ini link untuk mereset password anda <a href='" . base_url() . "auth/master/forgot/my?email=$email&token=" . urlencode($token) . "'>klik tautan ini</a>";
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    private function _logout()
    {
        $this->session->unset_userdata('email');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        <i class="feather icon-check mr-1"></i> Anda berhasil keluar.
        </div>');
        redirect('auth/master');
    }

    private function _createCaptcha()
    {
        $options = array(
            'img_path'      => './assets/captcha/',
            'img_url'       => base_url('assets/captcha'),
            'img_width'     => '150',
            'img_height'    => '30',
            'expiration'    => 7200
        );

        $cap = create_captcha($options);
        $image = $cap['image'];

        $this->session->set_userdata('captchaword', $cap['word']);

        return $image;
    }

    private function _checkCaptcha()
    {
        if ($this->input->post('captcha') == $this->session->userdata('captchaword')) {
            return true;
        } else {
            $this->form_validation->set_message('check_captcha', 'Captcha is wrong!');

            return false;
        }
    }
}
