<?php

class M_car extends CI_Model
{

    public function get_cars()
    {
        $this->db->select('*');
        $this->db->from('cars');
        $this->db->join('customer', 'customer.id_customer = cars.id_user');
        return $this->db->get();
    }

    public function resultCar($city = null, $vendor = null, $total_passenger = null)
    {
        $this->db->select('*');
        $this->db->from('cars');
        $this->db->join('customer', 'customer.id_customer = cars.id_user');
        $this->db->where('city', $city);
        $this->db->where('vendor', $vendor);
        $this->db->where('total_passenger', $total_passenger);
        return $this->db->get();
    }


    public function get_car($id_car)
    {
        $this->db->select('*');
        $this->db->from('cars');
        $this->db->join('customer', 'customer.id_customer = cars.id_user');
        $this->db->where('id_car', $id_car);
        return $this->db->get();
    }

    public function getAllVendor()
    {
        $this->db->select('vendor');
        $this->db->from('cars');
        $this->db->group_by('vendor');
        return $this->db->get();
    }
}
