-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 15, 2020 at 06:49 PM
-- Server version: 8.0.20-0ubuntu0.20.04.1
-- PHP Version: 7.3.18-1+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fastbook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` tinyint(1) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `remember_me` varchar(255) DEFAULT NULL,
  `joined_at` bigint DEFAULT NULL,
  `update_at` bigint DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `email`, `password`, `role_id`, `no_hp`, `is_active`, `remember_me`, `joined_at`, `update_at`, `avatar`) VALUES
(2, 'computercyber', 'cc.umrah@gmail.com', '$2y$10$Sg/1zmJuRFwV7sQedNZtwO7GoxSDzMhIzX2lAhbsYocQwUcdZM.jO', 2, '0822843422', 1, NULL, 1591548979, NULL, 'default.jpg'),
(3, 'Dimas Nugroho Putro', 'dimasnugroho673@gmail.com', '$2y$10$Sg/1zmJuRFwV7sQedNZtwO7GoxSDzMhIzX2lAhbsYocQwUcdZM.jO', 1, '082313814124', 1, NULL, 1591549198, NULL, 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id_booking` int NOT NULL,
  `id_invoice` varchar(255) DEFAULT NULL,
  `id_customer` int DEFAULT NULL,
  `id_seller` int DEFAULT NULL,
  `id_car` int DEFAULT NULL,
  `date_booking` bigint DEFAULT NULL,
  `until_date_booking` bigint DEFAULT NULL,
  `price_payment` bigint NOT NULL,
  `transfer_bank` varchar(255) DEFAULT NULL,
  `is_cancel` tinyint(1) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `is_new_booking` int DEFAULT NULL,
  `date_booking_update` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id_booking`, `id_invoice`, `id_customer`, `id_seller`, `id_car`, `date_booking`, `until_date_booking`, `price_payment`, `transfer_bank`, `is_cancel`, `status`, `is_new_booking`, `date_booking_update`) VALUES
(17, 'TNJG6eBs', 3, 1, 2, 1589216400, 1589562000, 540000, 'bri', NULL, 'waiting payment', 0, NULL),
(18, 'TNJSGt27', 4, 2, 3, 1589821200, 1590166800, 1000000, 'bri', NULL, 'waiting payment', 0, NULL),
(19, 'TNJXeqXT', 3, 2, 3, 1589302800, 1590685200, 1000000, 'bri', NULL, 'waiting payment', 0, NULL),
(20, 'TNJba7s6', 3, 2, 3, 1589302800, 1590685200, 1000000, 'bri', NULL, 'waiting payment', 0, NULL),
(21, 'TNJmNf6e', 3, 2, 3, 1589302800, 1590685200, 1000000, 'bri', NULL, 'waiting payment', 0, NULL),
(22, 'TNJpT7uL', 3, 1, 2, 1590426000, 1590771600, 540000, 'bri', NULL, 'success payment', 0, NULL),
(23, 'TNJUEQjP', 3, 1, 1, 1589821200, 1593190800, 650000, 'bri', NULL, 'success payment', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id_car` int NOT NULL,
  `plat_number` varchar(255) DEFAULT NULL,
  `name_car` varchar(255) DEFAULT NULL,
  `car_category` tinyint DEFAULT NULL,
  `total_passenger` int DEFAULT NULL,
  `year_of_assembly` int DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `ac` tinyint(1) DEFAULT NULL,
  `day_running_light` tinyint(1) DEFAULT NULL,
  `abs` tinyint(1) DEFAULT NULL,
  `transmission` varchar(255) DEFAULT NULL,
  `machine_type` varchar(255) DEFAULT NULL,
  `price` mediumint DEFAULT NULL,
  `description` text,
  `picture` varchar(255) DEFAULT NULL,
  `is_ready` varchar(255) DEFAULT NULL,
  `id_user` int DEFAULT NULL,
  `date_publish` int DEFAULT NULL,
  `date_upload` bigint DEFAULT NULL,
  `visitor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id_car`, `plat_number`, `name_car`, `car_category`, `total_passenger`, `year_of_assembly`, `vendor`, `color`, `ac`, `day_running_light`, `abs`, `transmission`, `machine_type`, `price`, `description`, `picture`, `is_ready`, `id_user`, `date_publish`, `date_upload`, `visitor`) VALUES
(1, 'BP 7726 MO', 'Ertiga', 1, 8, 2019, 'Suzuki', 'Abu-abu', 1, 1, 0, 'Automatic', 'Bensin', 650000, 'Ini adalah mobil pertama saya', 'profile_dimas.jpg', 'ready', 1, 1591896446, 1591896446, NULL),
(2, 'BP 23618 OI', 'Avanza', 1, 8, 2015, 'Toyota', 'Hitam', 1, 0, 0, 'Automatic', 'Bensin', 540000, 'Mobil avanza', NULL, 'ready', 1, 1587136830, 1587136830, NULL),
(3, 'BP XX823 OI', 'Xpander', 1, 8, 2018, 'Suzuki', 'black', 1, 1, 1, 'Automatic', 'Bensin', 1000000, 'wdsfSD', NULL, 'ready', 2, 1589002106, 1589002106, NULL),
(4, 'BP 7726 eEF', 'yaris', 1, 4, 2019, 'Toyota', 'Abu-abu', 1, 1, 1, 'Automatic', 'Diesel', 7689, 'hgctfgc', NULL, 'ready', 1, 1591893110, 1591893110, NULL),
(5, 'BP 7726 IOP', 'lamborgini', 1, 6, 0, 'daihatsu', '', 0, 0, 0, '', '', 2345245, '', NULL, 'ready', 1, 1589806370, 1589806370, NULL),
(6, 'BP 7726 IOPasd', 'jip', 1, 0, 0, 'honda', '', 0, 0, 0, '', '', 2344, '', NULL, 'ready', 1, 1589806459, 1589806459, NULL),
(7, '123423re', 'lamborgini231', 1, 0, 0, 'honda', '', 0, 0, 0, '', '', 0, '', NULL, 'ready', 1, 1589811446, 1589811446, NULL),
(8, '234', 'halu', 1, 0, 0, 'rino', '', 0, 0, 0, '', '', 3423523, '', NULL, 'ready', 1, 1589811541, 1589811541, NULL),
(9, '345', 'halu2', 1, 0, 0, 'toyota', '', 0, 0, 0, '', '', 0, '', NULL, 'ready', 1, 1589811641, 1589811641, NULL),
(10, '321', 'q2', 1, 0, 0, 'honda', '', 0, 0, 0, '', '', 0, '', NULL, 'ready', 1, 1589812218, 1589812218, NULL),
(11, '23423', 'lamborginiew3r', 1, 0, 0, 'suzuki', '', 0, 0, 0, '', '', 0, '', NULL, 'ready', 1, 1589812311, 1589812311, NULL),
(12, '2340', '75', 1, 0, 0, 'suzuki', '', 0, 0, 0, '', '', 0, '', NULL, 'ready', 1, 1589814668, 1589814668, NULL),
(14, 'BP 192334 PAS', 'New Grand Avanza', 1, 8, 2019, 'Toyota', 'Abu-abu', 1, 1, 1, 'Manual', 'Bensin', 900000, 'Mobil termantul ini', NULL, 'ready', 1, 1591891181, 1591891181, NULL),
(15, 'BP 192334 PAS', 'New Grand Avanza 2 ', 1, 8, 2019, 'Toyota', 'Abu-abu', 1, 1, 1, '', 'Bensin', 900000, 'Mobil termantul ini se tanjungpinang', NULL, 'ready', 1, 1591891772, 1591891772, NULL),
(16, 'BP 192334 PASapsan', 'New Grand Avanza 6', 1, 8, 2019, 'Toyota', 'Abu-abu', 1, 1, 1, '', 'Bensin', 900000, 'Mobil termantul ini se toapaya', NULL, 'ready', 1, 1591893326, 1591893326, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_category`
--

CREATE TABLE `car_category` (
  `id_category` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_category`
--

INSERT INTO `car_category` (`id_category`, `title`, `date_created`) VALUES
(1, 'Mobil', NULL),
(2, 'Pickup', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int NOT NULL,
  `name_customer` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `customer_type` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` text,
  `nik` bigint DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `account_type` varchar(255) DEFAULT NULL,
  `password_customer` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `first_join` bigint DEFAULT NULL,
  `token_active_start` bigint DEFAULT NULL,
  `token_active_end` bigint DEFAULT NULL,
  `is_trial` tinyint(1) DEFAULT NULL,
  `trial_end` bigint DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_verify` tinyint(1) DEFAULT NULL,
  `first_login` bigint DEFAULT NULL,
  `reason_join` varchar(255) DEFAULT NULL,
  `information_update` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `name_customer`, `username`, `customer_type`, `merchant_name`, `province`, `city`, `address`, `nik`, `email`, `avatar`, `no_hp`, `account_type`, `password_customer`, `token`, `first_join`, `token_active_start`, `token_active_end`, `is_trial`, `trial_end`, `is_active`, `is_verify`, `first_login`, `reason_join`, `information_update`) VALUES
(1, 'Mobil Bintan Perkasa', 'mobilbintanpr', 'seller', 'Mobil Bintan Perkasa', 'kepri', 'bintan', NULL, NULL, 'dimasnugroho673@gmail.com', NULL, '082285592029', 'premium', '$2y$10$tonZkQrnGnp9n38rWeMTieLPNxtDfvy4Z/35Q4rlFObsm/xFnSae.', 'LE1CBR5', 1587050614, 1591948182, 1623052181, 0, 1602887400, 1, 0, NULL, 'suka aja wkwkwk', NULL),
(2, 'Pinang Automobil', 'pinangautomobil', 'seller', 'pinangautomobil', 'kepri', 'tanjungpinang', NULL, NULL, 'pinangautomobil@gmail.com', NULL, NULL, 'trial', '$2y$10$tonZkQrnGnp9n38rWeMTieLPNxtDfvy4Z/35Q4rlFObsm/xFnSae.', NULL, 1587050614, NULL, NULL, 1, 1602887400, 1, 0, NULL, NULL, NULL),
(3, 'Oky purwanto', 'okypurwanto27', 'customer', NULL, 'kepri', 'bintan', NULL, NULL, 'okypurwanto81@gmail.com', NULL, '0975356557', NULL, '$2y$10$Sg/1zmJuRFwV7sQedNZtwO7GoxSDzMhIzX2lAhbsYocQwUcdZM.jO', NULL, 1587050614, NULL, NULL, 0, 1602887400, 1, 0, NULL, NULL, NULL),
(4, 'Sulthan SHP', 'sulthan.shp13', 'customer', NULL, 'kepri', 'tanjungpinang', NULL, NULL, 'sulthan.shp.13@gmail.com', NULL, '0975356557', NULL, '$2y$10$tonZkQrnGnp9n38rWeMTieLPNxtDfvy4Z/35Q4rlFObsm/xFnSae.', NULL, 1587050614, NULL, NULL, 0, 1602887400, 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_code`
--

CREATE TABLE `discount_code` (
  `id_discount_code` int NOT NULL,
  `discount_code` varchar(128) DEFAULT NULL,
  `discount_percent` float DEFAULT NULL,
  `date_start` bigint DEFAULT NULL,
  `date_end` bigint DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `discount_code`
--

INSERT INTO `discount_code` (`id_discount_code`, `discount_code`, `discount_percent`, `date_start`, `date_end`, `date_created`) VALUES
(1, 'NEWNORMAL20', 0.6, 1592326800, 1593018000, 1591543475),
(3, 'CORONA23', 0.4, 1593363600, 1593536400, 1591543508);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id_invoice` varchar(255) NOT NULL,
  `from_customer` int DEFAULT NULL,
  `for_seller` int DEFAULT NULL,
  `date_want_booking` bigint DEFAULT NULL,
  `date_payment` bigint DEFAULT NULL,
  `limit_date_payment` bigint DEFAULT NULL,
  `transfer_bank` varchar(255) DEFAULT NULL,
  `status_payment` varchar(129) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `is_new_invoice` int DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id_invoice`, `from_customer`, `for_seller`, `date_want_booking`, `date_payment`, `limit_date_payment`, `transfer_bank`, `status_payment`, `is_new_invoice`, `date_created`) VALUES
('TNJba7s6', 3, 2, 1589302800, NULL, 1589702037, 'bri', 'waiting', 0, 1589615637),
('TNJG6eBs', 3, 1, 1589216400, NULL, 1589209595, 'bri', 'waiting', 0, 1589123195),
('TNJmNf6e', 3, 2, 1589302800, NULL, 1589702041, 'bri', 'waiting', 0, 1589615641),
('TNJPcovK', 3, 1, 1589821200, NULL, 1589879733, 'bri', 'waiting', 0, 1589793333),
('TNJpT7uL', 3, 1, 1590426000, 1589629649, 1589702061, 'bri', 'success', 0, 1589615661),
('TNJSGt27', 4, 2, 1589821200, NULL, 1589209595, 'bri', 'waiting', 0, 1589123203),
('TNJtQA5E', 3, 1, 1588611600, NULL, 1589879720, 'bri', 'waiting', 0, 1589793320),
('TNJUEQjP', 3, 1, 1589821200, 1589793394, 1589879771, 'bri', 'success', 0, 1589793371),
('TNJXeqXT', 3, 2, 1589302800, NULL, 1589209595, 'bri', 'waiting', 0, 1589615628);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id_picture` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `picture_car` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `picture_seller` int DEFAULT NULL,
  `token_picture` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `token_customer`
--

CREATE TABLE `token_customer` (
  `id_token_customer` int NOT NULL,
  `stock_token` varchar(255) DEFAULT NULL,
  `long_month` bigint DEFAULT NULL,
  `date_start` mediumint DEFAULT NULL,
  `date_end` mediumint DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `customer_using` int DEFAULT NULL,
  `date_activation` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `token_customer`
--

INSERT INTO `token_customer` (`id_token_customer`, `stock_token`, `long_month`, `date_start`, `date_end`, `status`, `customer_using`, `date_activation`) VALUES
(1, '5S36R9O', 6, NULL, NULL, 'preactive', NULL, NULL),
(2, 'IB07D9H', 6, NULL, NULL, 'preactive', NULL, NULL),
(3, 'Z0A8K6B', 6, NULL, NULL, 'preactive', NULL, NULL),
(4, 'YW0HMSR', 6, NULL, NULL, 'preactive', NULL, NULL),
(5, 'RNXSGJG', 6, NULL, NULL, 'preactive', NULL, NULL),
(6, 'K08VWM9', 6, NULL, NULL, 'preactive', NULL, NULL),
(7, '6M7UJLC', 6, NULL, NULL, 'preactive', NULL, NULL),
(8, '3V8D5EW', 6, NULL, NULL, 'preactive', NULL, NULL),
(9, 'OVLLRW4', 6, NULL, NULL, 'preactive', NULL, NULL),
(10, 'Z9AWEIQ', 6, NULL, NULL, 'preactive', NULL, NULL),
(11, 'EH9L85S', 12, NULL, NULL, 'preactive', NULL, NULL),
(12, 'Y4IPWPA', 12, NULL, NULL, 'preactive', NULL, NULL),
(13, 'PPR2IXS', 12, NULL, NULL, 'preactive', NULL, NULL),
(14, 'GJFLXN5', 12, NULL, NULL, 'preactive', NULL, NULL),
(15, 'M00FNSC', 12, NULL, NULL, 'preactive', NULL, NULL),
(16, 'MSYNQBE', 12, NULL, NULL, 'preactive', NULL, NULL),
(17, '8T1TZEB', 12, NULL, NULL, 'preactive', NULL, NULL),
(18, '3CUCH0P', 12, NULL, NULL, 'preactive', NULL, NULL),
(19, 'LE1CBR5', 12, NULL, NULL, 'active', 1, 1591948182),
(20, 'AUIR12T', 12, NULL, NULL, 'preactive', NULL, NULL),
(21, 'V0WFRBW', 24, NULL, NULL, 'preactive', NULL, NULL),
(22, '436TMZD', 24, NULL, NULL, 'preactive', NULL, NULL),
(23, 'BPB03FX', 24, NULL, NULL, 'preactive', NULL, NULL),
(24, 'SX45R9M', 24, NULL, NULL, 'preactive', NULL, NULL),
(25, 'PA420SA', 24, NULL, NULL, 'preactive', NULL, NULL),
(26, 'AXASZ2H', 24, NULL, NULL, 'preactive', NULL, NULL),
(27, 'V0G2YTN', 24, NULL, NULL, 'preactive', NULL, NULL),
(28, '02E9A6T', 24, NULL, NULL, 'preactive', NULL, NULL),
(29, 'VGAJFAT', 24, NULL, NULL, 'preactive', NULL, NULL),
(30, 'YIRZW2Z', 24, NULL, NULL, 'preactive', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_forgot_password_token`
--

CREATE TABLE `user_forgot_password_token` (
  `user_forgot_pass_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email_user` varchar(255) DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_registration_token`
--

CREATE TABLE `user_registration_token` (
  `user_reg_token` varchar(255) NOT NULL,
  `email_user` varchar(255) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `date_created` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_registration_token`
--

INSERT INTO `user_registration_token` (`user_reg_token`, `email_user`, `role_id`, `date_created`) VALUES
('cPkelobjUD76IeKoJh3Sl1y+KEmzPCJ6RhHn18Mu21w=', 'dimasnugroho673@gmail.com', 4, 1591592832),
('EN2zBg2+YxBV93gYFRI1I1e3FtmtK8qt3uTPvDaFzrQ=', 'dimasnugroho673@gmail.com', 5, 1591592764);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id_user_role` int NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id_user_role`, `role_name`) VALUES
(1, 'Super Administrator'),
(2, 'Administrator'),
(3, 'Direktur'),
(4, 'Manajer'),
(5, 'Karyawan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id_car`);

--
-- Indexes for table `car_category`
--
ALTER TABLE `car_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `discount_code`
--
ALTER TABLE `discount_code`
  ADD PRIMARY KEY (`id_discount_code`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id_picture`);

--
-- Indexes for table `token_customer`
--
ALTER TABLE `token_customer`
  ADD PRIMARY KEY (`id_token_customer`);

--
-- Indexes for table `user_forgot_password_token`
--
ALTER TABLE `user_forgot_password_token`
  ADD PRIMARY KEY (`user_forgot_pass_token`);

--
-- Indexes for table `user_registration_token`
--
ALTER TABLE `user_registration_token`
  ADD PRIMARY KEY (`user_reg_token`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id_user_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id_booking` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id_car` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `car_category`
--
ALTER TABLE `car_category`
  MODIFY `id_category` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `discount_code`
--
ALTER TABLE `discount_code`
  MODIFY `id_discount_code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id_picture` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `token_customer`
--
ALTER TABLE `token_customer`
  MODIFY `id_token_customer` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id_user_role` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
