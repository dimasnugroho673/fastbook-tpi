<div class="auth-wrapper">
    <div class="blur-bg-images"></div>

    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="card-body">
                        <!-- <div class="text-center">
                            <img src="assets/images/logo-dark.png" alt="" class="img-fluid mb-4">
                        </div> -->
                        <h4 class="mb-4 f-w-400">Change your password</h4>

                        <form action="<?= site_url('auth/master/forgot/my?email=' . $this->input->get('email') . '&token=' . urlencode($this->input->get('token'))) ?>" method="post">
                            <div class="input-group mb-3">
                                <input type="password" name="password1" class="form-control rounded-right <?php echo (form_error('password1')) ? "is-invalid" : null; ?>" placeholder="Password">
                                <div class="invalid-feedback">
                                    <?= form_error('password1') ?>
                                </div>
                            </div>
                            <div class="input-group mb-4">
                                <input type="password" name="password2" class="form-control rounded-right <?php echo (form_error('password2')) ? "is-invalid" : null; ?>" placeholder="Retype password">
                                <div class="invalid-feedback">
                                    <?= form_error('password2') ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary mb-4">Change password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>