<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Car extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
        $data['cars']       = $this->db->get_where('cars', array('id_user' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer))->result();
        $data['view_page'] = 'car/index';

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/layout/sidebar', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }

    public function add()
    {
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
        $data['cars']       = $this->db->get('cars')->result();
        $data['view_page'] = 'car/add';

        $this->form_validation->set_rules('plat_number', 'Nomor Plat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('userpanel/layout/header', $data);
            $this->load->view('userpanel/layout/sidebar', $data);
            $this->load->view('userpanel/layout/footer', $data);
        } else {

            $config['upload_path']         = './assets/uploads/cars/cover/';  //lokasi folder upload
            $config['allowed_types']     = 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
            $config['overwrite']            = true;
            $config['max_size']            = 2048; // KB	
            // $config['max_width']            = 1366;
            // $config['max_height']           = 768;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('picture')) {
                echo "pesan error : " .  $this->upload->display_errors();

                // Masuk database 
            } else {
                $upload_data                = array('uploads' => $this->upload->data());


                $data = array(
                    'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                    'name_car'          => htmlspecialchars($this->input->post('name_car', TRUE)),
                    'car_category'      => 1,
                    'total_passenger'   => htmlspecialchars($this->input->post('total_passenger', TRUE)),
                    'year_of_assembly'  => htmlspecialchars($this->input->post('year_of_assembly', TRUE)),
                    'vendor'            => htmlspecialchars($this->input->post('vendor', TRUE)),
                    'color'             => htmlspecialchars($this->input->post('color', TRUE)),
                    'ac'                => htmlspecialchars($this->input->post('ac', TRUE)),
                    'day_running_light' => htmlspecialchars($this->input->post('day_running_light', TRUE)),
                    'abs'               => htmlspecialchars($this->input->post('abs', TRUE)),
                    'transmission'      => htmlspecialchars($this->input->post('transmission', TRUE)),
                    'machine_type'      => htmlspecialchars($this->input->post('machine_type', TRUE)),
                    'price'             => htmlspecialchars($this->input->post('price', TRUE)),
                    'description'       => htmlspecialchars($this->input->post('description', TRUE)),
                    'picture'           => $upload_data['uploads']['file_name'],
                    'is_ready'          => 'ready',
                    'id_user'           => $data['check_user']->id_customer,
                    'date_publish'      => time(),
                    'date_upload'       => time(),
                );


                $this->db->insert('cars', $data);


                $search_car_db = $this->db->get_where('cars', array(
                    'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                    'id_user'           => $data['check_user']->id_customer,
                ))->row();
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Mobil baru berhasil ditambah
            </div>');
                $this->session->set_userdata(array(
                    'id_car' => $search_car_db->id_car,
                ));
                redirect('panel/car/ap?id=' . urlencode(encrypt_url($search_car_db->id_car)));
            }
        }
    }

    public function edit($id_car)
    {

        $get_id_car = urldecode(decrypt_url($id_car));

        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
        $data['car']       = $this->db->get_where('cars', array('id_car' => $get_id_car))->row();
        $data['pictures_car'] = $this->db->get_where('pictures', array('picture_car' => $get_id_car))->result();
        $data['view_page'] = 'car/edit';

        $this->form_validation->set_rules('plat_number', 'Nomor Plat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('userpanel/layout/header', $data);
            $this->load->view('userpanel/layout/sidebar', $data);
            $this->load->view('userpanel/layout/footer', $data);
        } else {

            if (!empty($_FILES['picture']['name'])) {
                $config['upload_path']         = './assets/uploads/cars/cover/';  //lokasi folder upload
                $config['allowed_types']     = 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
                $config['overwrite']            = true;
                $config['max_size']            = 2048; // KB	
                // $config['max_width']            = 1366;
                // $config['max_height']           = 768;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('picture')) {
                    echo "pesan error : " .  $this->upload->display_errors();

                    // Masuk database 
                } else {
                    $upload_data                = array('uploads' => $this->upload->data());


                    $dataUpdate = array(
                        'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                        'name_car'          => htmlspecialchars($this->input->post('name_car', TRUE)),
                        'car_category'      => 1,
                        'total_passenger'   => htmlspecialchars($this->input->post('total_passenger', TRUE)),
                        'year_of_assembly'  => htmlspecialchars($this->input->post('year_of_assembly', TRUE)),
                        'vendor'            => htmlspecialchars($this->input->post('vendor', TRUE)),
                        'color'             => htmlspecialchars($this->input->post('color', TRUE)),
                        'ac'                => htmlspecialchars($this->input->post('ac', TRUE)),
                        'day_running_light' => htmlspecialchars($this->input->post('day_running_light', TRUE)),
                        'abs'               => htmlspecialchars($this->input->post('abs', TRUE)),
                        'transmission'      => htmlspecialchars($this->input->post('transmission', TRUE)),
                        'machine_type'      => htmlspecialchars($this->input->post('machine_type', TRUE)),
                        'price'             => htmlspecialchars($this->input->post('price', TRUE)),
                        'description'       => htmlspecialchars($this->input->post('description', TRUE)),
                        'picture'           => $upload_data['uploads']['file_name'],
                        'is_ready'          => 'ready',
                        'id_user'           => $data['check_user']->id_customer,
                        'date_publish'      => time(),
                        'date_upload'       => time(),
                    );


                    $this->db->where('id_car', $get_id_car);
                    $this->db->update('cars', $dataUpdate);


                    $search_car_db = $this->db->get_where('cars', array(
                        'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                        'id_user'           => $data['check_user']->id_customer,
                    ))->row();
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Berhasil mengganti data mobil
                 </div>');
                    $this->session->set_userdata(array(
                        'id_car' => $search_car_db->id_car,
                    ));
                    redirect('panel/car');
                }
            } else {
                $dataUpdate = array(
                    'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                    'name_car'          => htmlspecialchars($this->input->post('name_car', TRUE)),
                    'car_category'      => 1,
                    'total_passenger'   => htmlspecialchars($this->input->post('total_passenger', TRUE)),
                    'year_of_assembly'  => htmlspecialchars($this->input->post('year_of_assembly', TRUE)),
                    'vendor'            => htmlspecialchars($this->input->post('vendor', TRUE)),
                    'color'             => htmlspecialchars($this->input->post('color', TRUE)),
                    'ac'                => htmlspecialchars($this->input->post('ac', TRUE)),
                    'day_running_light' => htmlspecialchars($this->input->post('day_running_light', TRUE)),
                    'abs'               => htmlspecialchars($this->input->post('abs', TRUE)),
                    'transmission'      => htmlspecialchars($this->input->post('transmission', TRUE)),
                    'machine_type'      => htmlspecialchars($this->input->post('machine_type', TRUE)),
                    'price'             => htmlspecialchars($this->input->post('price', TRUE)),
                    'description'       => htmlspecialchars($this->input->post('description', TRUE)),
                    'is_ready'          => 'ready',
                    'id_user'           => $data['check_user']->id_customer,
                    'date_publish'      => time(),
                    'date_upload'       => time(),
                );


                $this->db->where('id_car', $get_id_car);
                $this->db->update('cars', $dataUpdate);


                $search_car_db = $this->db->get_where('cars', array(
                    'plat_number'       => htmlspecialchars($this->input->post('plat_number', TRUE)),
                    'id_user'           => $data['check_user']->id_customer,
                ))->row();
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil mengganti data mobil
             </div>');
                $this->session->set_userdata(array(
                    'id_car' => $search_car_db->id_car,
                ));
                redirect('panel/car');
            }
        }
    }

    public function ap()
    {
        if ($this->input->get('id') == null) {
            echo "hai";
            die;
        } else {
            $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
            $data['cars']       = $this->db->get_where('cars', array('id_user' => $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer))->result();
            $data['view_page'] = 'car/upload_picture';

            $this->load->view('userpanel/layout/header', $data);
            $this->load->view('userpanel/layout/sidebar', $data);
            $this->load->view('userpanel/layout/footer', $data);
        }
    }

    public function detail($id_car)
    {
        $get_id_car = urldecode(decrypt_url($id_car));

        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();
        $data['detail_car']       = $this->db->get_where('cars', array(
            'id_car' => $get_id_car
        ))->row();

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/car/detail', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }

    // AJAX Request

    public function uploadPicture($id_car = null)
    {
        // $id_car = $this->session->userdata('id_car');
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();

        $config['upload_path']   = FCPATH . '/assets/uploads/cars/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|ico';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $title = $this->upload->data('file_name');
            $token = $this->input->post('token_picture');
            $this->db->insert('pictures', array(
                'title'             => $title,
                'picture_car'       => $id_car,
                'picture_seller'     => $data['check_user']->id_customer,
                'token_picture'     => $token,
                'date_created'      => time()
            ));
        }
    }

    //Untuk menghapus foto
    public function removePicture()
    {

        //Ambil token foto
        $token_picture = $this->input->post('token');

        $picture = $this->db->get_where('pictures', array('token_picture' => $token_picture));


        if ($picture->num_rows() > 0) {
            $get_pict = $picture->row();
            $title = $get_pict->title;
            if (file_exists($file = FCPATH . '/assets/uploads/cars/' . $title)) {
                unlink($file);
            }
            $this->db->delete('pictures', array('token_picture' => $token_picture));
        }


        echo $token_picture;
    }

    public function delete_pictures_car($token_picture, $id_car)
    {
        $get_id_car = urldecode(decrypt_url($id_car));

        $get_picture_on_db = $this->db->get_where('pictures', array(
            'token_picture' => $token_picture
        ))->row();

        if (file_exists($file = FCPATH . '/assets/uploads/cars/' . $get_picture_on_db->title)) {
            unlink($file);

            $this->db->delete('pictures', array(
                'id_picture' => $get_picture_on_db->id_picture
            ));

            redirect('panel/car/edit/' . $id_car);
        } else {
            echo "failed";
        }
    }



    // AJAX request
    public function delete_picture()
    {
        $src = $this->input->post('src');

        if (file_exists($file = FCPATH . '/assets/uploads/cars/cover/' . $src)) {
            unlink($file);

            $this->db->set('picture', null);
            $this->db->update('cars');

            echo "Berhasil menghapus gambar";
        } else {
            echo "failed";
        }
    }

    // public function delete_pictures()
    // {
    //     $src = $this->input->post('src');
    //     $id_picture = $this->input->post('id_picture');

    //     echo "gas";
    //     die;

    //     if (file_exists($file = FCPATH . '/assets/uploads/cars/' . $src)) {
    //         unlink($file);

    //         $this->db->set('picture', null);
    //         $this->db->update('cars');

    //         echo "Berhasil menghapus gambar";
    //     } else {
    //         echo "failed";
    //     }
    // }
}
