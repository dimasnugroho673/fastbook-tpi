<?php

class M_token extends CI_Model
{

    private $table = 'token_customer';

    public function adminTokenListing()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->get();
    }

    public function getSendToken($long_month)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('status', 'preactive');
        $this->db->where('long_month', $long_month);
        $this->db->limit(1);
        $this->db->order_by('stock_token', 'RANDOM');
        return $this->db->get();
    }
}
