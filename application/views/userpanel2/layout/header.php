<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Car Rentals</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
			CSS
            ============================================= -->
    <link href="<?php echo base_url('assets/user/dependency/datatables/css/jquery.dataTables.min.css') ?>" rel="stylesheet">

    <script src="<?= base_url('assets/user/template/'); ?>js/vendor/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url('assets/user/dependency/datatables/js/jquery.dataTables.min.js') ?>"></script>


    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/linearicons.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/nice-select.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/animate.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('assets/user/template/'); ?>css/main.css">


</head>

<body>

    <header id="header" id="home">
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="index.html">Fastbook</a>
                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="menu-active"><a href="index.html">Home</a></li>
                        <li><a href="cars.html">Mobil</a></li>
                        <li><a href="service.html">Angkutan Barang</a></li>
                        <li><a href="about.html">About</a></li>
                        <li class="menu-has-children ml-4">
                            <img src="https://buildwithangga.com/themes/front/images/logo-bwa.png" alt="..." class="rounded-circle d-xs-none" width="30px">
                            <a href="#"><?= $check_user->username; ?></a>
                            <ul class="rounded">
                                <li><a href="">Profile</a></li>
                                <li><a href="">Daftar Mobil</a></li>
                                <li><a href="">Transaksi</a></li>
                                <li><a href="">Setelan</a></li>
                                <li><a href="<?= site_url('logout'); ?>">Keluar</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </div>
    </header><!-- #header -->

    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Panel Saya
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Panel </a> <span class="lnr lnr-arrow-right"></span> <a href="" class="text-capitalize"> <?= $this->uri->segment(2); ?></a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->