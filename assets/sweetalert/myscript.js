// hapus agenda
$('.btn-delete-discount_code').on('click', function (e) {

	e.preventDefault();
	const href = $(this).attr('href');

	Swal.fire({
		title: 'Yakin ingin menghapus',
		text: "Apa kamu yakin ingin menghapus?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Hapus kode!'
	}).then((result) => {
		if (result.value) {
			document.location.href = href;
		}
	})
});
