<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_booking');

        $this->db->set('is_new_booking', 0);
        $this->db->where('id_seller', $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row()->id_customer);
        $this->db->update('booking');
    }

    public function index()
    {
        $data['check_user'] = $this->db->get_where('customer', array('email' => $this->session->userdata('email')))->row();


        $data['bookings'] = $this->M_booking->seller_listing($data['check_user']->id_customer)->result();
        $data['view_page'] = 'booking/index';

        $this->load->view('userpanel/layout/header', $data);
        $this->load->view('userpanel/layout/sidebar', $data);
        $this->load->view('userpanel/layout/footer', $data);
    }
}
